#!/usr/bin/env python
import time
import rospy
import sys
from tensorflow.contrib import tensorrt as tftrt
import copy
from std_msgs.msg import Float32, Int8
from sensor_msgs.msg import Joy
from sensor_msgs.msg import CompressedImage, Image
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
import cv2
import xuly
import numpy as np
from Controler.xuly import Controler
import Config
from keras.models import load_model
import stream
import tensorflow as tf
import keras
from keras import backend as K

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
keras.backend.tensorflow_backend.set_session(tf.Session(config=config))



# cv2.imshow=stream.imshow

class TfEngine(object):
  def __init__(self, graph):
    g = tf.Graph()
    with g.as_default():
      x_op, y_op = tf.import_graph_def(
          graph_def=graph.frozen, return_elements=graph.x_name + graph.y_name)
      self.x_tensor = x_op.outputs[0]
      self.y_tensor = y_op.outputs[0]

    config = tf.ConfigProto(gpu_options=
      tf.GPUOptions(per_process_gpu_memory_fraction=0.5,
      allow_growth=True))

    self.sess = tf.Session(graph=g, config=config)

  def infer(self, x):
    y = self.sess.run(self.y_tensor,
      feed_dict={self.x_tensor: x})
    return y

class FrozenGraph(object):
    def __init__(self, model, shape):
        shape = (None, shape[0], shape[1], shape[2])
        x_name = 'image_tensor_x'
        with K.get_session() as sess:
            x_tensor = tf.placeholder(tf.float32, shape, x_name)
            K.set_learning_phase(0)
            y_tensor = model(x_tensor)
            y_name = y_tensor.name[:-2]
            graph = sess.graph.as_graph_def()
            graph0 = tf.graph_util.convert_variables_to_constants(sess, graph, [y_name])
            graph1 = tf.graph_util.remove_training_nodes(graph0)

        self.x_name = [x_name]
        self.y_name = [y_name]
        self.frozen = graph1


class TftrtEngine(TfEngine):
    def __init__(self, graph, batch_size, precision):
        tftrt_graph = tftrt.create_inference_graph(
            graph.frozen,
            outputs=graph.y_name,
            max_batch_size=batch_size,
            max_workspace_size_bytes=1 << 30,
            precision_mode=precision,
            minimum_segment_size=2)

        opt_graph = copy.deepcopy(graph)
        opt_graph.frozen = tftrt_graph
        super(TftrtEngine, self).__init__(opt_graph)
        self.batch_size = batch_size

    def infer(self, x):
        num_tests = x.shape[0]
        y = np.empty((num_tests, 2,256,256), np.float32)
        batch_size = self.batch_size

        for i in range(0, num_tests, batch_size):
            x_part = x[i: i + batch_size]
            y_part = self.sess.run(self.y_tensor,
                                   feed_dict={self.x_tensor: x_part})
            y[i: i + batch_size] = y_part
        return y


class Node_Controller:
    def __init__(self):
        self.bridge = CvBridge()
        self.speed = 0
        self.steer = 0
        self.mode = 0
        self.rgb_frame = None
        self.traffic_sign = 0

        self.joy_sub = rospy.Subscriber("/joy", Joy, self.joyCallback, queue_size=1)
        self.speed_pub = rospy.Publisher("/set_speed_car_api", Float32, queue_size=1)
        self.steer_pub = rospy.Publisher("/set_steer_car_api", Float32, queue_size=1)
        self.camera_steer_pub = rospy.Publisher("/set_pos_camera_api", Float32, queue_size=1)
        rospy.Subscriber("camera/rgb/image_raw/compressed", CompressedImage, self.rgbCallback, queue_size=1)
        rospy.Subscriber("traffic_sign", Int8, self.trafficCallback, queue_size=1)

    def rgbCallback(self, rgb_data):
        np_arr = np.fromstring(rgb_data.data, np.uint8)
        self.rgb_frame = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

    def joyCallback(self, ros_data):
        if ros_data.buttons[14] == 1:
            # Nhan nut X thi ko dk duoc nua
            self.mode = 0
        elif ros_data.buttons[13] == 1:
            # Nhan nut O thi enable dieu khien xe bang ps3
            self.mode = 1
        elif ros_data.buttons[12] == 1:
            # Nhan nut tam giac thi enable mode xe tu lai
            self.mode = 2

    def trafficCallback(self, ros_data):
        data = ros_data.data
        if data == 0:  ### re phai
            self.traffic_sign = 1
        elif data == 1:  ## re trai
            self.traffic_sign = -1
        else:
            self.traffic_sign = 0


def main(args):
    rospy.init_node("binh", anonymous=True)
    node_controller = Node_Controller()
    controler = Controler()
    # model = load_model('model_binary/mobilenet.h5')
    # frozen_graph =
    tftrt_engine = TftrtEngine(FrozenGraph(load_model('model_binary/mobilenet.h5'), (128, 128, 3)), 1, 'FP32')
    print('Finish initModel')
    try:
        while not rospy.is_shutdown():
            speed = 0
            angle = 0
            start_time = time.time()
            if node_controller.rgb_frame is not None:
                #### xu ly chinh ####
		
                mat = node_controller.rgb_frame
                mat = cv2.resize(mat, (128, 128)) / 255.
                y = tftrt_engine.infer(np.array([mat]))[0]
                # y = model.predict(np.array([mat]))[0]
                road = y[0]
                line = y[1]
                road[road>0.5] = 1
                road[road<=0.5] = 0
                # road[road>0.5] = 1
                segment = np.zeros((256, 256, 3))
                segment[road > 0.5] = (0, 255, 0)
                segment[line > 0.5] = (0, 255, 255)
                try:
			cv2.imshow('segment',segment)
			angle, speed = controler.run2(road, 1)
		except:
			speed = 12
                if Config.debug:
                    cv2.imshow('road', road)

                    cv2.waitKey(1)
                # speed = 10

            if node_controller.mode == 2:
                #### neu dang o mode xe tu lai
                print(speed, angle)
                node_controller.speed_pub.publish(speed)
                node_controller.steer_pub.publish(angle)
            elif node_controller.mode == 0:
                node_controller.speed_pub.publish(0.0)
                node_controller.steer_pub.publish(0.0)
            # print('error')

            print("FPS: ", 1.0 / (time.time() - start_time))
        # rate.sleep()
        rospy.spin()
    except KeyboardInterrupt:
        print("keyboard interrupt")


if __name__ == '__main__':
    main(sys.argv)
