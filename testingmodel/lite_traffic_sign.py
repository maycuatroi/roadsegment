import glob
import random
import cv2
import keras
import numpy as np
from keras.callbacks import Callback
from keras.engine import Layer
from keras.layers import Dense, Flatten, Input, Concatenate, Permute, \
    AveragePooling2D, Conv2D, Dropout, MaxPooling2D, UpSampling2D, BatchNormalization, ELU, Reshape, Activation
from keras.models import Model

from Generator.lite_traffic_generator2 import TrafficLiteGenerator

data_source = r'D:\Video_Log/'
label_paths = glob.glob(data_source + 'labels/*.png')


class My_Callback(Callback):
    def __init__(self, generator):
        self.generator = generator

    def on_epoch_end(self, epoch, logs={}):
        bath = next(self.generator(5))

        # image = bath[0][0]
        y = self.model.predict(bath[0])
        for i, mat in enumerate(bath[0]):
            mat *= 255.
            label = ['back ground', 'left', 'right'][np.argmax(y[i])]

            cv2.imwrite('log/{}_{}_{}.png'.format(epoch, label, i), mat)
        self.model.save_weights('weights/lite_traffic.h5')
        return


def traffic_lite_net():
    road_input = Input((32, 32, 3))
    conv1 = Conv2D(8, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv1')(road_input)
    conv1 = Dropout(0.2)(conv1)
    conv1 = Conv2D(8, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv2')(conv1)

    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(16, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv3')(pool1)
    conv2 = Dropout(0.2)(conv2)
    conv2 = Conv2D(16, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv4')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv5')(pool2)
    conv3 = Dropout(0.2)(conv3)
    x = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv6')(conv3)

    x = Flatten()(x)
    x = Dense(32)(x)
    x = Dense(16)(x)
    x = Dense(3, activation='softmax')(x)
    model = Model(inputs=road_input, outputs=x)
    model.compile(loss=keras.losses.categorical_crossentropy, optimizer='adam', metrics=['accuracy'])
    return model


# def pun_net_reconstruction():


model = traffic_lite_net()
model.summary()
model.load_weights('weights/lite_traffic.h5')
model.save('weights/traffic_lite_model.h5')
lite_generator = TrafficLiteGenerator(r'D:\Video_Log\32x32')

callback = My_Callback(lite_generator.generator)
# model.fit_generator(lite_generator.generator(64), steps_per_epoch=200, epochs=20, callbacks=[callback])
cap = cv2.VideoCapture(r"D:\log\src\1552899392.08_rgb.avi")
while (1):

    ret, original = cap.read()
    # original = np.flip(original,1)
    original = cv2.flip(original,1)
    if not ret:
        cap = cv2.VideoCapture(r"D:\log\src\1552899392.08_rgb.avi")
        continue
    # image = cv2.imread(random.choice(image_paths))
    image = original.copy()
    mask = cv2.inRange(cv2.cvtColor(image, cv2.COLOR_RGB2HSV), (12, 16, 124), (34, 183, 255))
    im2, contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    candidates = []
    boxs = []
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w / h > 2 or h / w > 2 or w + h < 50 or w + h > 200:
            continue
        boxs.append((x, y, w, h))
        candidate = image[y:y + h, x:x + w]
        candidate = cv2.resize(candidate, (32, 32))
        candidates.append(candidate)
    acc = 0
    cv2.imshow('mask', mask)
    can_mat = None
    for can in candidates:
        if can_mat is None:
            can_mat = can
        else:
            can_mat = np.concatenate((can_mat, can), 1)
    if can_mat is None:
        continue
    cv2.imshow('candidates', can_mat)
    batch = np.array(candidates) / 255.
    predicted = model.predict(batch)
    for i in range(len(candidates)):
        index = np.argmax(predicted[i])
        acc = predicted[i][index]
        if acc < 0.5:
            continue
        label = ['back ground', 'left', 'right'][index]

        color = (0, 255, 0)
        if label == 'left':
            color = (0, 0, 255)
        if label in ['left', 'right']:
            x, y, w, h = boxs[i]
            cv2.rectangle(original, (x, y), (x + w, y + h), color, 1)
            cv2.drawContours(original, contours, i, color, 1)
        else:
            acc = 0
        print(label,acc)
    cv2.imshow('original', original)
    if acc > 0.5:

        cv2.waitKey(0)
    else:
        cv2.waitKey(1)
