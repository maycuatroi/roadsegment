from __future__ import division

import glob

import cv2
import random

import numpy as np
from keras.callbacks import Callback
from keras.optimizers import Adam, SGD

from Modules.PunLoss import PunLoss
from Generator.SSDGenerator import SSDGenerator
from ssd_ultis.keras_ssd300 import ssd_300
from ssd_ultis.ssd_encoder_decoder.ssd_input_encoder import SSDInputEncoder
from ssd_ultis.ssd_encoder_decoder.ssd_output_decoder import decode_detections





class My_Callback(Callback):

    def __init__(self,test_image_paths=None):
        test_image_paths = glob.glob(test_image_paths+'/*.png')
        self.testimages_path = test_image_paths

    def on_epoch_end(self, epoch, logs={}):
        image_name = random.choice(self.testimages_path)

        image = cv2.imread(image_name)
        h, w, c = image.shape
        resized = cv2.resize(image, (300, 300)) / 255.
        y = self.model.predict(np.array([resized]))
        y_pred_decoded = decode_detections(y,
                                           confidence_thresh=0.5,
                                           iou_threshold=0.45,
                                           top_k=200,
                                           normalize_coords=False,
                                           img_height=h,
                                           img_width=w)
        if len(y_pred_decoded[0] > 0):
            line = y_pred_decoded[0][0]
            if line[1] > 0:
                label = 'None'
                if line[0] == 1:
                    label = 'left'
                elif line[0] == 2:
                    label = 'right'

                cv2.putText(image, label, (int(line[2]), int(line[3])), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2,
                            (0, 255, 255), 2, cv2.LINE_AA)
                cv2.imwrite('log/{}.jpg'.format(epoch), image)

            np.set_printoptions(precision=2, suppress=True, linewidth=90)
        self.model.save_weights('weights/traffic_300.h5')

        print("Predicted boxes:\n")
        print('   class   conf xmin   ymin   xmax   ymax')
        print(y_pred_decoded[0])
        return


if __name__ == '__main__':

    img_height = 300
    img_width = 300
    img_channels = 3
    mean_color = [123, 117, 104]
    swap_channels = [2, 1, 0]
    n_classes = 2
    scales = [0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05]
    aspect_ratios = [[1.0, 2.0, 0.5],
                     [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                     [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                     [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                     [1.0, 2.0, 0.5],
                     [1.0, 2.0, 0.5]]
    two_boxes_for_ar1 = True
    steps = None#[8, 16, 32, 64, 100, 300]
    offsets = None#[0.5, 0.5, 0.5, 0.5, 0.5, 0.5]
    clip_boxes = False
    variances = [0.1, 0.1, 0.2, 0.2]
    normalize_coords = True

    model = ssd_300(image_size=(img_height, img_width, img_channels),
                            n_classes=n_classes,
                            mode='training',
                            l2_regularization=0.0005,
                            scales=scales,
                            aspect_ratios_per_layer=aspect_ratios,
                            two_boxes_for_ar1=two_boxes_for_ar1,
                            steps=steps,
                            offsets=offsets,
                            clip_boxes=clip_boxes,
                            variances=variances,
                            normalize_coords=normalize_coords,
                            subtract_mean=mean_color,
                            swap_channels=swap_channels)

    model.summary()
    callback = My_Callback(test_image_paths='/home/sap_lab/workspace/video_7')
    adam =  Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    sgd = SGD(lr=0.001, momentum=0.9, decay=0.0, nesterov=False)

    pun_loss = PunLoss(neg_pos_ratio=3, alpha=1.0)

    model.compile(optimizer=adam, loss=pun_loss.compute_context_loss)


    predictor_sizes = [model.get_layer('conv4_3_norm_mbox_conf').output_shape[1:3],
                       model.get_layer('fc7_mbox_conf').output_shape[1:3],
                       model.get_layer('conv6_2_mbox_conf').output_shape[1:3],
                       model.get_layer('conv7_2_mbox_conf').output_shape[1:3],
                       model.get_layer('conv8_2_mbox_conf').output_shape[1:3],
                       model.get_layer('conv9_2_mbox_conf').output_shape[1:3]]

    encoder = SSDInputEncoder(img_height=img_height,
                                    img_width=img_width,
                                    n_classes=n_classes,
                                    predictor_sizes=predictor_sizes,
                                    scales=scales,
                                    aspect_ratios_per_layer=aspect_ratios,
                                    two_boxes_for_ar1=two_boxes_for_ar1,
                                    steps=steps,
                                    offsets=offsets,
                                    clip_boxes=clip_boxes,
                                    variances=variances,
                                    matching_type='multi',
                                    pos_iou_threshold=0.5,
                                    neg_iou_limit=0.5,
                                    normalize_coords=normalize_coords)
<<<<<<< HEAD
    ssd_generator = SSDGenerator(r'D:\Video_Log\traffic/',encoder)
=======
    ssd_generator = SSDGenerator('/home/sap_lab/workspace/video',encoder)
>>>>>>> b174537dedb364888e733c1cadd1bc9ec5209e05

    # model.load_weights('weights/traffic_300.h5')
    model.fit_generator(ssd_generator.generator(32), steps_per_epoch=200, epochs=100, callbacks=[callback])
