from __future__ import division

import glob

import cv2
import random

import numpy as np
from keras.callbacks import Callback
from keras.models import Model
from keras.layers import Input, Conv2D, MaxPooling2D, BatchNormalization, ELU, Reshape, Concatenate, Activation, \
    Dropout, UpSampling2D, Permute, Cropping2D
from keras.optimizers import Adam
from keras.regularizers import l2

import common
from ssd_ultis.ssd_encoder_decoder import SSDInputEncoder
from ssd_ultis.ssd_encoder_decoder import decode_detections
from ssd_ultis.keras_layers import AnchorBoxes
from xml.etree import ElementTree as ET
import tensorflow as tf
import image_processing as imgproc
from keras.utils.vis_utils import plot_model

config_data = common.getConfig("detectlane")
image_paths = glob.glob('/home/binhbumpro/catkin_ws/log/*.png')


def detectRoadwayByHLSImage(hls_image):
    _, _, s_channel = cv2.split(hls_image)

    s_channel_histogram = imgproc.getHistogram(s_channel)
    s_channel_fft_histogram = imgproc.getFFTHistogram(s_channel_histogram)

    local_minimums = imgproc.localMinimumOnFFTHistogram(s_channel_fft_histogram)

    return imgproc.inRange1Channel(s_channel, 0, local_minimums[0])


def process_image(image):
    # convert cac kenh mau khac nhau
    bgr_image = image
    gray_image = imgproc.convertBGRtoGray(bgr_image)
    hls_image = imgproc.convertBGRtoHLS(bgr_image)

    # tinh histogram, fft histogram va cac cuc tieu cua fft histogram
    gray_histogram = imgproc.getHistogram(gray_image)
    gray_fft_histogram = imgproc.getFFTHistogram(gray_histogram)

    # lay long duong bang anh HLS
    roadway_binary_image = detectRoadwayByHLSImage(hls_image)
    roadway_gray_image = cv2.bitwise_and(gray_image, gray_image, mask=roadway_binary_image)
    if (np.any(roadway_gray_image > 0)):
        roadway_gray_color_avg = int(roadway_gray_image[roadway_gray_image > 0].mean())
    else:
        roadway_gray_color_avg = 0

    # lay ra cac vung mau co dien tich lon, tim vung co dien tich lon nhat
    large_area_color = imgproc.getLargeAreaColorStrips(gray_histogram, gray_fft_histogram)
    largest_area_color = large_area_color[large_area_color[:, 4].argmax()]
    if (roadway_gray_color_avg >= largest_area_color[0] and roadway_gray_color_avg <= largest_area_color[2]):
        # roadway dang chiem nhieu dien tich nhat
        roadway_binary_image = imgproc.inRange1Channel(gray_image, largest_area_color[0], largest_area_color[2])
        roadway_binary_image = imgproc.erode(roadway_binary_image, (2, 2))
        roadway_binary_image = imgproc.findBiggestContour(roadway_binary_image)
        roadway_binary_image = imgproc.dilate(roadway_binary_image, (2, 2))

        shadow_binary_image = imgproc.findOtherArea(gray_image, 0, largest_area_color[0])
        shadow_binary_image = imgproc.erode(shadow_binary_image, (2, 2))
    elif (roadway_gray_color_avg < largest_area_color[0]):
        # dang huong ra ngoai
        shadow_binary_image = np.zeros_like(roadway_binary_image)
    else:
        # shadow dang chiem nhieu dien tich nhat
        shadow_binary_image = imgproc.inRange1Channel(gray_image, largest_area_color[0], largest_area_color[2])

        roadway_binary_image = imgproc.erode(roadway_binary_image, (2, 2))

    full_road = cv2.bitwise_or(roadway_binary_image, shadow_binary_image)
    full_road = imgproc.findBiggestContour(full_road)
    full_road = imgproc.erode(full_road, (5, 5))
    full_road = imgproc.findBiggestContour(full_road)
    full_road = imgproc.dilate(full_road, (5, 5))

    # cv2.imshow('test',np.concatenate((full_road,cv2.cvtColor(image,cv2.COLOR_RGB2GRAY)),1))

    return full_road


class PunLoss:
    def __init__(self,
                 neg_pos_ratio=3,
                 n_neg_min=0,
                 alpha=1.0,
                 sigma=0.5):
        self.neg_pos_ratio = neg_pos_ratio
        self.n_neg_min = n_neg_min
        self.alpha = alpha
        self.sigma = sigma

    def smooth_L1_loss(self, y_true, y_pred):
        absolute_loss = tf.abs(y_true - y_pred)
        square_loss = 0.5 * (y_true - y_pred) ** 2
        l1_loss = tf.where(tf.less(absolute_loss, 1.0), square_loss, absolute_loss - 0.5)
        return tf.reduce_sum(l1_loss, axis=-1)

    def log_loss(self, y_true, y_pred):
        y_pred = tf.maximum(y_pred, 1e-15)
        log_loss = -tf.reduce_sum(y_true * tf.log(y_pred), axis=-1)
        return log_loss

    def compute_loss(self, y_true, y_pred):
        self.neg_pos_ratio = tf.constant(self.neg_pos_ratio)
        self.n_neg_min = tf.constant(self.n_neg_min)
        self.alpha = tf.constant(self.alpha)

        batch_size = tf.shape(y_pred)[0]
        n_boxes = tf.shape(y_pred)[1]

        classification_loss = tf.to_float(self.log_loss(y_true[:, :, :-12], y_pred[:, :, :-12]))
        localization_loss = tf.to_float(self.smooth_L1_loss(y_true[:, :, -12:-8], y_pred[:, :, -12:-8]))
        negatives = y_true[:, :, 0]
        positives = tf.to_float(tf.reduce_max(y_true[:, :, 1:-12], axis=-1))
        n_positive = tf.reduce_sum(positives)

        pos_class_loss = tf.reduce_sum(classification_loss * positives, axis=-1)
        neg_class_loss_all = classification_loss * negatives
        n_neg_losses = tf.count_nonzero(neg_class_loss_all, dtype=tf.int32)
        n_negative_keep = tf.minimum(tf.maximum(self.neg_pos_ratio * tf.to_int32(n_positive), self.n_neg_min),
                                     n_neg_losses)

        def f1():
            return tf.zeros([batch_size])

        def f2():
            neg_class_loss_all_1D = tf.reshape(neg_class_loss_all, [-1])
            values, indices = tf.nn.top_k(neg_class_loss_all_1D,
                                          k=n_negative_keep,
                                          sorted=False)
            negatives_keep = tf.scatter_nd(indices=tf.expand_dims(indices, axis=1),
                                           updates=tf.ones_like(indices, dtype=tf.int32),
                                           shape=tf.shape(neg_class_loss_all_1D))
            negatives_keep = tf.to_float(tf.reshape(negatives_keep, [batch_size, n_boxes]))
            # ...and use it to keep only those boxes and mask all other classification losses
            neg_class_loss = tf.reduce_sum(classification_loss * negatives_keep, axis=-1)
            return neg_class_loss

        neg_class_loss = tf.cond(tf.equal(n_neg_losses, tf.constant(0)), f1, f2)
        class_loss = pos_class_loss + neg_class_loss
        loc_loss = tf.reduce_sum(localization_loss * positives, axis=-1)
        total_loss = (class_loss + self.alpha * loc_loss) / tf.maximum(1.0, n_positive)
        total_loss = total_loss * tf.to_float(batch_size)

        return total_loss


def build_model(image_size=(240, 320, 3),
                n_classes=3,
                l2_regularization=0.0,
                scales=[0.08, 0.16, 0.32, 0.64, 0.96],
                two_boxes_for_ar1=True,
                clip_boxes=False,
                variances=[1.0, 1.0, 1.0, 1.0],
                coords='centroids',
                normalize_coords=False,
                ):
    number_predictor = 4
    steps = [None] * number_predictor
    offsets = [None] * number_predictor
    n_classes += 1
    l2_reg = l2_regularization
    img_height, img_width, img_channels = image_size[0], image_size[1], image_size[2]

    x = Input(shape=(img_height, img_width, img_channels))
    crop = Cropping2D(cropping=((0, 84), (0, 0)), data_format='channels_last')(x)
    conv1 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same')(crop)
    conv1 = Dropout(0.2)(conv1)
    conv1 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same')(conv1)

    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same')(pool1)
    conv2 = Dropout(0.2)(conv2)
    conv2 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(128, (3, 3), strides=1, activation='relu', padding='same')(pool2)
    conv3 = Dropout(0.2)(conv3)
    conv3 = Conv2D(128, (3, 3), strides=1, activation='relu', padding='same')(conv3)

    up1 = Concatenate()([UpSampling2D(size=(2, 2))(conv3), conv2])
    conv4 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same')(up1)
    conv4 = Dropout(0.2)(conv4)
    conv4 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same')(conv4)

    up2 = Concatenate()([UpSampling2D(size=(2, 2))(conv4), conv1])
    conv5 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same')(up2)
    conv5 = Dropout(0.2)(conv5)
    conv5 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same')(conv5)

    conv6 = Conv2D(1, 1, strides=1, activation='sigmoid', padding='same')(conv5)
    outputs = Permute((3, 1, 2),name='segment_output')(conv6)

    conv1 = Conv2D(32, (5, 5), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv1')(x)
    conv1 = BatchNormalization(axis=3, momentum=0.99, name='bn1')(
        conv1)
    conv1 = ELU(name='elu1')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2), name='pool1')(conv1)

    conv2 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv2')(pool1)
    conv2 = BatchNormalization(axis=3, momentum=0.99, name='bn2')(conv2)
    conv2 = ELU(name='elu2')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2), name='pool2')(conv2)

    conv3 = Conv2D(64, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv3')(pool2)
    conv3 = BatchNormalization(axis=3, momentum=0.99, name='bn3')(conv3)
    conv3 = ELU(name='elu3')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2), name='pool3')(conv3)

    conv4 = Conv2D(64, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv4')(pool3)
    conv4 = BatchNormalization(axis=3, momentum=0.99, name='bn4')(conv4)
    conv4 = ELU(name='elu4')(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2), name='pool4')(conv4)

    conv5 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv5')(pool4)
    conv5 = BatchNormalization(axis=3, momentum=0.99, name='bn5')(conv5)
    conv5 = ELU(name='elu5')(conv5)
    pool5 = MaxPooling2D(pool_size=(2, 2), name='pool5')(conv5)

    conv6 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv6')(pool5)
    conv6 = BatchNormalization(axis=3, momentum=0.99, name='bn6')(conv6)
    conv6 = ELU(name='elu6')(conv6)
    pool6 = MaxPooling2D(pool_size=(2, 2), name='pool6')(conv6)

    conv7 = Conv2D(32, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv7')(pool6)
    conv7 = BatchNormalization(axis=3, momentum=0.99, name='bn7')(conv7)
    conv7 = ELU(name='elu7')(conv7)

    classes4 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes4')(conv4)
    classes5 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes5')(conv5)
    classes6 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes6')(conv6)
    classes7 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes7')(conv7)
    # Output shape of `boxes`: `(batch, height, width, n_boxes * 4)`
    boxes4 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes4')(conv4)
    boxes5 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes5')(conv5)
    boxes6 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes6')(conv6)
    boxes7 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes7')(conv7)

    anchors4 = AnchorBoxes(img_height, img_width, this_scale=scales[0], next_scale=scales[1],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[0], this_offsets=offsets[0],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors4')(boxes4)
    anchors5 = AnchorBoxes(img_height, img_width, this_scale=scales[1], next_scale=scales[2],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[1], this_offsets=offsets[1],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors5')(boxes5)
    anchors6 = AnchorBoxes(img_height, img_width, this_scale=scales[2], next_scale=scales[3],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[2], this_offsets=offsets[2],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors6')(boxes6)
    anchors7 = AnchorBoxes(img_height, img_width, this_scale=scales[3], next_scale=scales[4],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[3], this_offsets=offsets[3],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors7')(boxes7)

    classes4_reshaped = Reshape((-1, n_classes), name='classes4_reshape')(classes4)
    classes5_reshaped = Reshape((-1, n_classes), name='classes5_reshape')(classes5)
    classes6_reshaped = Reshape((-1, n_classes), name='classes6_reshape')(classes6)
    classes7_reshaped = Reshape((-1, n_classes), name='classes7_reshape')(classes7)
    boxes4_reshaped = Reshape((-1, 4), name='boxes4_reshape')(boxes4)
    boxes5_reshaped = Reshape((-1, 4), name='boxes5_reshape')(boxes5)
    boxes6_reshaped = Reshape((-1, 4), name='boxes6_reshape')(boxes6)
    boxes7_reshaped = Reshape((-1, 4), name='boxes7_reshape')(boxes7)
    anchors4_reshaped = Reshape((-1, 8), name='anchors4_reshape')(anchors4)
    anchors5_reshaped = Reshape((-1, 8), name='anchors5_reshape')(anchors5)
    anchors6_reshaped = Reshape((-1, 8), name='anchors6_reshape')(anchors6)
    anchors7_reshaped = Reshape((-1, 8), name='anchors7_reshape')(anchors7)
    classes_concat = Concatenate(axis=1, name='classes_concat')([classes4_reshaped,
                                                                 classes5_reshaped,
                                                                 classes6_reshaped,
                                                                 classes7_reshaped])
    boxes_concat = Concatenate(axis=1, name='boxes_concat')([boxes4_reshaped,
                                                             boxes5_reshaped,
                                                             boxes6_reshaped,
                                                             boxes7_reshaped])
    anchors_concat = Concatenate(axis=1, name='anchors_concat')([anchors4_reshaped,
                                                                 anchors5_reshaped,
                                                                 anchors6_reshaped,
                                                                 anchors7_reshaped])
    classes_softmax = Activation('softmax', name='classes_softmax')(classes_concat)
    predictions = Concatenate(axis=2, name='detection_output')([classes_softmax, boxes_concat, anchors_concat])

    model = Model(inputs=x, outputs=[predictions, outputs])

    return model


image_paths = glob.glob('/home/binhbumpro/Downloads/annotation/boxes_box_stone_20190111/all/*.png')
class_idx = {'box': 1, 'stone': 2, 'boxes': 3}


def read_annotation(path):
    tree = ET.parse(path)
    root = tree.getroot()
    for p in root.findall('.//object'):
        id = class_idx[p.find('name').text]
        xmin = int(p.find('bndbox').find('xmin').text)
        ymin = int(p.find('bndbox').find('ymin').text)
        xmax = int(p.find('bndbox').find('xmax').text)
        ymax = int(p.find('bndbox').find('ymax').text)
        return id, xmin, ymin, xmax, ymax


def generator(batch_size, ssd_input_encoder=None):
    batch_images = np.zeros((batch_size, 240, 320, 3))
    batch_segment = np.zeros((batch_size, 1, 156, 320))
    while True:
        annotations = []
        for i in range(batch_size):
            image_name = random.choice(image_paths)
            mat = cv2.imread(image_name)
            croped = mat[84:240, :]
            id, xmin, ymin, xmax, ymax = read_annotation(image_name.replace('.png', '.xml'))
            annotations.append(np.array([[id, xmin, ymin, xmax, ymax]]))
            batch_images[i] = mat/ 255.
            batch_segment[i] = [process_image(croped) / 255]
        if ssd_input_encoder is not None:
            batch_annotions = ssd_input_encoder(annotations)
        else:
            batch_annotions = annotations
        yield batch_images, [batch_annotions, batch_segment]


class My_Callback(Callback):

    def on_epoch_end(self, epoch, logs={}):
        image = next(generator(1))[0][0]

        y = self.model.predict(np.array([image]))
        # print(y)
        # print(y.shape)
        seg = y[1][0][0]*255
        seg = cv2.cvtColor(seg,cv2.COLOR_GRAY2RGB)
        y_pred_decoded = decode_detections(y[0],
                                           confidence_thresh=0.1,
                                           iou_threshold=0.45,
                                           top_k=200,
                                           normalize_coords=False,
                                           img_height=240,
                                           img_width=320)
        if len(y_pred_decoded[0] > 0):
            line = y_pred_decoded[0][0]
            image *= 255
            # print(image.shape,y[0][0].shape)
            if line[1] > 0:
                cv2.rectangle(image, (int(line[2]), int(line[3])), (int(line[4]), int(line[5])), (0, 255, 0), 2)
                cv2.imwrite('log/{}.jpg'.format(epoch), np.concatenate((image,seg)))
                self.model.save_weights('pun_net.h5')
            np.set_printoptions(precision=2, suppress=True, linewidth=90)
        # print("Predicted boxes:\n")
        # print('   class   conf xmin   ymin   xmax   ymax')
        # print(y_pred_decoded[0])
        return


if __name__ == '__main__':
    model = build_model()
    model.summary()
    callback = My_Callback()
    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

    ssd_focal_loss = PunLoss(neg_pos_ratio=3, alpha=1.0, sigma=0.5)
    predictor_sizes = [model.get_layer('classes4').output_shape[1:3],
                       model.get_layer('classes5').output_shape[1:3],
                       model.get_layer('classes6').output_shape[1:3],
                       model.get_layer('classes7').output_shape[1:3]]

    losses = {
        "detection_output": ssd_focal_loss.compute_loss,
        "segment_output": "categorical_crossentropy",
    }
    lossWeights = {"detection_output": 1.0, "segment_output": 0.3}

    model.compile(optimizer=adam, loss=losses, loss_weights=lossWeights,
                  metrics=["accuracy"])

    plot_model(model, to_file='pun_net.png', show_shapes=True, show_layer_names=True)
    encoder = SSDInputEncoder(img_height=240,
                              img_width=320,
                              n_classes=3,
                              predictor_sizes=predictor_sizes)

    model.load_weights('pun_net.h5')
    model.fit_generator(generator(16, encoder), steps_per_epoch=500, epochs=100, callbacks=[callback])
