import glob
import random

import cv2
import keras
import numpy as np
from keras.applications import MobileNetV2
from keras.callbacks import Callback
from keras.engine import Layer
from keras.layers import Dense, Flatten, Input, Concatenate, Permute, \
    AveragePooling2D, Conv2D, Dropout, MaxPooling2D, UpSampling2D, BatchNormalization, ELU, Reshape, Activation
from keras.models import Model
# from imgaug import augmenters as iaa
# import imgaug as ia
import threading
import time
import tensorflow as tf
from keras.regularizers import l2
import keras.backend as K
from ssd_ultis.keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from ssd_ultis.keras_layers.keras_layer_DecodeDetections import DecodeDetections


class ThreadingBatches(object):

    def __init__(self, generator=None, fast_generator=None):
        self.generator = generator
        self.stack = []
        self.fast_generator = fast_generator
        self.queue_size = 20
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True  # Daemonize thread
        thread.start()  # Start the execution

    def run(self):
        while True:
            if (len(self.stack) < self.queue_size):
                try:
                    batch = next(self.generator)
                    self.stack.append(batch)
                except ValueError:
                    None

    def popNextBatch(self):
        if len(self.stack) == 0:
            time.sleep(0.1)
            return next(self.fast_generator)

        return self.stack.pop(0)


data_source = r'F:\Video_Log/'
label_paths = glob.glob(data_source + 'labels/*.png')


def load_segment(label_name):
    segment = cv2.imread(label_name, 0)
    return segment


# seq = iaa.Sequential([
#     iaa.Crop(px=(0, 3)),  # crop images from each side by 0 to 16px (randomly chosen)
#     iaa.Fliplr(0.5),  # horizontally flip 50% of the images
#     iaa.PerspectiveTransform(0.04),
#     iaa.PiecewiseAffine(0.04),
#     iaa.Snowflakes((0.05, 0.25)),
#     # iaa.Affine(rotate=(-45, 45))
# ])


def generator(batch_size):
    batch_features = np.zeros((batch_size, 100, 100, 3))
    batch_segment = np.zeros((batch_size, 2, 400, 400))
    while True:
        seq_det = seq.to_deterministic()
        mats = []
        segs = []

        for i in range(batch_size):
            label_name = random.choice(label_paths)
            image_name = label_name.replace('labels', 'images')
            if '-' in image_name:
                image_name = image_name.replace('png', 'jpg')
            mat = cv2.imread(image_name)
            mats.append(mat)
            segment = load_segment(label_name)
            segment = cv2.resize(segment, (400, 400))
            segmap = ia.SegmentationMapOnImage(segment, shape=segment.shape, nb_classes=3)
            segs.append(segmap)

        mats = seq_det.augment_images(np.array(mats))
        segs = seq_det.augment_segmentation_maps(np.array(segs))

        for i in range(len(mats)):
            segment = segs[i].arr
            road, line, bg = cv2.split(np.round(segment))
            road = 1 - road
            mat = mats[i]
            mat = cv2.resize(mat, (100, 100))
            batch_features[i] = mat
            batch_segment[i] = np.array([road, line])
        yield batch_features / 255., batch_segment


def generator_no_augment(batch_size):
    batch_features = np.zeros((batch_size, 224, 224, 3))
    batch_segment = np.zeros((batch_size, 3, 224, 224))
    while True:
        for i in range(batch_size):
            label_name = random.choice(label_paths)
            image_name = label_name.replace('labels', 'images')
            if '-' in image_name:
                image_name = image_name.replace('png', 'jpg')
            mat = cv2.imread(image_name)
            # if bool(random.getrandbits(1)):
            #     mat = cv2.resize(mat, (random.randint(30, 100), random.randint(30, 100)))
            mat = cv2.resize(mat, (224, 224))
            segment = load_segment(label_name)
            segment = cv2.resize(segment, (224, 224))


            if bool(random.getrandbits(1)):
                segment = cv2.flip(segment, 0)
                mat = cv2.flip(mat, 0)

            road = np.zeros((224, 224))
            line = np.zeros((224, 224))
            bg = np.zeros((224, 224))

            road[segment == 2] = 1
            line[segment == 1] = 1
            bg[segment == 0] = 1

            batch_features[i] = mat
            batch_segment[i] = np.array([bg, road, line])
        yield batch_features / 255., batch_segment


# thread_train = ThreadingBatches(generator(5), generator_no_augment(5))


def thread_generator():
    while True:
        yield thread_train.popNextBatch()


# while True:
#     bath = next(generator(5))
#     segment =  np.zeros((400,400,3))
#     road_mat = bath[1][0][0]
#     line_mat = bath[1][0][1]
#     segment[road_mat==1]=(255,0,0)
#     segment[line_mat==1]=(0,255,0)
#     image = cv2.resize(bath[0][0],(400,400))
#     cv2.imshow('image',np.concatenate((image,segment),1))
#     # cv2.imshow('road',segment)
#     cv2.waitKey(0)

class My_Callback(Callback):
    def __init__(self):
        self.test_images = glob.glob(r'F:\Video_Log\new_image\*.png')
        self.colors = [(0, 0, 0), (0, 255, 255), (0, 255, 0)]

    def on_epoch_end(self, epoch, logs={}):
        original = cv2.imread(random.choice(self.test_images))
        original = cv2.resize(original, (224, 224))
        image = original / 255.

        y = self.model.predict(np.array([image]))

        # road = y[0][1]
        # bg = y[0][0]
        # line = y[0][2]
        y = np.argmax(y[0], 0)

        segment = np.zeros((224, 224, 3), dtype=np.uint8)
        segment[y == 0] = self.colors[0]
        segment[y == 1] = self.colors[1]
        segment[y == 2] = self.colors[2]

        original = cv2.resize(original, (224, 224))
        cv2.imwrite('log/{}.jpg'.format(epoch), np.concatenate((original, segment), 1))
        self.model.save_weights('weights/punnet_mobile.h5')
        return



def image_func(img):
    img = cv2.resize(img, (100, 100))
    return img.astype('float32')


def image_tensor_func(img4d):
    results = []
    for img3d in img4d:
        rimg3d = image_func(img3d)
        results.append(np.expand_dims(rimg3d, axis=0))
    return np.concatenate(results, axis=0)


class Custom_resize_Layer(Layer):
    def call(self, xin):
        xout = tf.py_func(image_tensor_func,
                          [xin],
                          'float32',
                          stateful=False,
                          name='cvOpt')
        xout = K.stop_gradient(xout)  # explicitly set no grad
        xout.set_shape([xin.shape[0], 100, 100, xin.shape[-1]])  # explicitly set output shape
        return xout

    def compute_output_shape(self, sin):
        return (sin[0], 100, 100, sin[-1])



def pun_net():
    road_input = Input((224, 224, 3))
    # semi_input = Conv2D(3,(3,3))
    base_model = MobileNetV2(
        include_top=False,
        input_tensor=road_input,
        input_shape=(224,224, 3),
        pooling='max')


    for layer in base_model.layers:
        layer.trainable = True  # trainable has to be false in order to freeze the layers

    x = base_model.get_layer('block_13_expand_relu').output
    up2 = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('block_6_expand_relu').output])
    x = Conv2D(64,(3,3), padding='same')(up2)
    x = Dropout(0.2)(x)

    x = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('block_3_expand_relu').output])
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = Dropout(0.2)(x)
    # x = BatchNormalization()(x)

    x = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('block_1_expand_relu').output])
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = Dropout(0.2)(x)
    # x = BatchNormalization()(x)

    x = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('input_1').output])
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = Dropout(0.2)(x)
    # x = BatchNormalization()(x)

    # x = UpSampling2D(size=(2, 2))(x)
    x = Conv2D(64,(3,3), padding='same')(x)
    x = Conv2D(3,1,activation='softmax')(x)
    # x = Permute((3,1,2))(x)
    x = Permute((3,2,1))(x)
    model = Model(road_input,x)
    model.compile(loss=keras.losses.categorical_crossentropy, optimizer='adam', metrics=['accuracy'])

    return model



# def pun_net_reconstruction():


model = pun_net()
model.summary()
# model.load_weights('weights/punnet_mobile.h5')
callback = My_Callback()

model.fit_generator(generator_no_augment(16), steps_per_epoch=500, epochs=1000, callbacks=[callback])
model.save('model_binary/punnet_tiny.hdf5')
# video_path = r"D:\video\1551349372.26_rgb.avi"
# cap = cv2.VideoCapture(video_path)


# while True:
#     # image = next(generator(1))[0][0]
#     ret, mat = cap.read()
#     if not ret:
#         cap = cv2.VideoCapture(video_path)
#         continue
#         # break
#     sign_image = cv2.resize(mat, (300, 300)) / 255.
#     road_image = cv2.resize(mat, (100, 100)) / 255.
#     # y = model.predict([np.array([road_image]),np.array([sign_image])])
#     y = model.predict(np.array([road_image]))
#     model.save('weights/punnet_extend.h5')
#     # y_seg = y[0]
#     # y_sign = y[1]
#
#     # y= y_seg
#     road = y[0][0]
#     line = y[0][1]
#     image = cv2.resize(mat, (400, 400))
#     segment = np.zeros((400, 400, 3))
#     segment[road > 0.5] = (0, 255, 0)
#     segment[line > 0.5] = (0, 255, 255)
#     segment = segment.astype(np.uint8)
#     # image = np.concatenate((image,road),1)
#     image = (image * 255.).astype(np.uint8)
#     mat = cv2.resize(mat, (400, 400))
#     out_image = np.concatenate((mat, segment), 1)
#     cv2.imshow('test', out_image)
#     k = cv2.waitKey(1)
#     # if k ==ord('s'):
#     #     writeout = True
#     if k == 32:
#         cv2.waitKey(0)
