import glob
import random

import cv2
import numpy as np
from keras.callbacks import Callback
from keras.layers import Dense, Flatten, Input, Concatenate, Permute, \
    AveragePooling2D, Conv2D, Dropout, MaxPooling2D, UpSampling2D
from keras.models import Model

image_paths = glob.glob(r'F:\Video_Log\unlabeled/*.jpg')


def load_segment(label_name):
    segment= cv2.imread(label_name,0)

    return segment


def generator(batch_size):
    batch_features = np.zeros((batch_size, 480, 480,3))
    batch_segment = np.zeros((batch_size,480, 480,3))
    while True:
        for i in range(batch_size):
            # choose random index in features
            # index = random.choice(len(image_paths), 1)
            image_path = random.choice(image_paths)
            mat = cv2.imread(image_path)

            # mat = mat / 255.
            mat_feature = cv2.resize(mat,(480,480))/255.
            mat_out = cv2.resize(mat,(480,480))/255.
            batch_features[i] = mat_feature
            # b,g,r =cv2.split(mat)
            batch_segment[i] = mat_out
        yield batch_features, batch_segment

# while True:
#     bath = next(generator(1))
#
#     cv2.imshow('image',bath[0][0])
#     cv2.imshow('segment',bath[1][0])
#     cv2.waitKey(0)

class My_Callback(Callback):

    def on_epoch_end(self, epoch, logs={}):
        bath = next(generator(1))
        image = bath[0][0]

        y = self.model.predict(np.array([image]))
        image *= 255.
        y *= 255.
        out = y[0]

        # segment[segment!=0] = 255.
        # print(image.shape,y[0][0].shape)
        # out = np.reshape(out,(100,100,3))
        image = cv2.resize(image,(480,480))
        cv2.imwrite('log/{}.jpg'.format(epoch), np.concatenate((image,out),1))
        self.model.save_weights('weights/road_autoencoder.h5')
        return


def pun_net():
    image_input = Input((480, 480,3))
    # crop = Cropping2D(cropping=((0, 84), (0, 0)), data_format='channels_last')(image_input)

    conv1 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv1')(image_input)
    conv1 = Dropout(0.2)(conv1)
    conv1 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv2')(conv1)

    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv3')(pool1)
    conv2 = Dropout(0.2)(conv2)
    conv2 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv4')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(128, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv5')(pool2)
    conv3 = Dropout(0.2)(conv3)
    conv3 = Conv2D(128, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv6')(conv3)

    up1 = Concatenate()([UpSampling2D(size=(2, 2))(conv3), conv2])
    conv4 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv7')(up1)
    conv4 = Dropout(0.2)(conv4)
    conv4 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv8')(conv4)

    up2 = Concatenate()([UpSampling2D(size=(2, 2))(conv4), conv1])
    conv5 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv9')(up2)
    conv5 = Dropout(0.2)(conv5)
    conv5 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv10')(conv5)

    # up3 = UpSampling2D(size=(2, 2))(conv5)
    # conv6 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same')(up3)
    # conv6 = Dropout(0.2)(conv6)
    # x = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same')(conv6)

    # x = UpSampling2D(size=(2, 2))(x)
    # x = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same')(x)
    # x = Dropout(0.2)(x)
    # x = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same')(x)

    conv6 = Conv2D(3, 1, strides=1, activation='sigmoid', padding='same')(conv5)
    # outputs = Permute((3, 1, 2))(conv6)
    model = Model(input=image_input, output=conv6)

    model.compile(loss="mse", optimizer='adam', metrics=['accuracy'])

    return model





model = pun_net()
model.summary()
# model.load_weights('weights/road_autoencoder.h5')
callback = My_Callback()

model.fit_generator(generator(5), steps_per_epoch=100, epochs=100, callbacks=[callback])
model.save_weights('weights/road_segment_autoencoder.h5')

# from keras.utils.vis_utils import plot_model
# plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True)
cap = cv2.VideoCapture(r"F:\Video_Log\7.avi")
out_vid = cv2.VideoWriter('output2.avi',cv2.VideoWriter_fourcc(*'XVID'), 24.0, (800,400))
writeout= False
while True:
    # image = next(generator(1))[0][0]
    ret,image = cap.read()
    if not ret:
        break
    # image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
    image = cv2.resize(image,(100,100))/255.
    y =model.predict(np.array([image]))
    # image *= 255.
    # y *= 255.
    out = y[0]

    # segment[segment!=0] = 255.
    # print(image.shape,y[0][0].shape)
    # out = np.reshape(out,(100,100,3))
    image = cv2.resize(image,(400,400))
    cv2.putText(image,'original',(10,20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,(0,255,255),1,cv2.LINE_AA)
    cv2.putText(out,'anti-aliased',(10,20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,(0,255,255),1,cv2.LINE_AA)
    output= np.concatenate((image,out),1)
    cv2.imshow('test',output )
    k=cv2.waitKey(1)
    if k ==ord('s'):
        writeout = True

    if writeout:
        out_vid.write((output*255.).astype(np.uint8) )

out_vid.release()