from __future__ import division

import glob
import json

import cv2
import random

import numpy as np
from keras.callbacks import Callback
from keras.models import Model
from keras.layers import Input, Conv2D, MaxPooling2D, BatchNormalization, ELU, Reshape, Concatenate, Activation
from keras.optimizers import Adam
from keras.regularizers import l2

from xml.etree import ElementTree as ET
import tensorflow as tf
import os.path

from Modules.PunLoss import PunLoss
from ssd_ultis.keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from ssd_ultis.keras_layers.keras_layer_DecodeDetections import DecodeDetections
from ssd_ultis.ssd_encoder_decoder.ssd_input_encoder import SSDInputEncoder
from ssd_ultis.ssd_encoder_decoder.ssd_output_decoder import decode_detections




def build_model(image_size=(300, 300, 3),
                n_classes=2,
                mode='training',
                l2_regularization=0.0,
                scales=[0.08, 0.16, 0.32, 0.64, 0.96],
                two_boxes_for_ar1=True,
                clip_boxes=False,
                variances=[1.0, 1.0, 1.0, 1.0],
                coords='centroids',
                normalize_coords=False,
                confidence_thresh=0.01,
                iou_threshold=0.45,
                top_k=200,
                nms_max_output_size=400,
                return_predictor_sizes=False):
    number_predictor = 4
    steps = [None] * number_predictor
    offsets = [None] * number_predictor
    n_classes += 1
    l2_reg = l2_regularization
    img_height, img_width, img_channels = image_size[0], image_size[1], image_size[2]

    x = Input(shape=(img_height, img_width, img_channels))
    conv1 = Conv2D(32, (5, 5), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv1')(x)
    conv1 = BatchNormalization(axis=3, momentum=0.99, name='bn1')(
        conv1)
    conv1 = ELU(name='elu1')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2), name='pool1')(conv1)

    conv2 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv2')(pool1)
    conv2 = BatchNormalization(axis=3, momentum=0.99, name='bn2')(conv2)
    conv2 = ELU(name='elu2')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2), name='pool2')(conv2)

    conv3 = Conv2D(64, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv3')(pool2)
    conv3 = BatchNormalization(axis=3, momentum=0.99, name='bn3')(conv3)
    conv3 = ELU(name='elu3')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2), name='pool3')(conv3)

    conv4 = Conv2D(64, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv4')(pool3)
    conv4 = BatchNormalization(axis=3, momentum=0.99, name='bn4')(conv4)
    conv4 = ELU(name='elu4')(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2), name='pool4')(conv4)

    conv5 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv5')(pool4)
    conv5 = BatchNormalization(axis=3, momentum=0.99, name='bn5')(conv5)
    conv5 = ELU(name='elu5')(conv5)
    pool5 = MaxPooling2D(pool_size=(2, 2), name='pool5')(conv5)

    conv6 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv6')(pool5)
    conv6 = BatchNormalization(axis=3, momentum=0.99, name='bn6')(conv6)
    conv6 = ELU(name='elu6')(conv6)
    pool6 = MaxPooling2D(pool_size=(2, 2), name='pool6')(conv6)

    conv7 = Conv2D(32, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv7')(pool6)
    conv7 = BatchNormalization(axis=3, momentum=0.99, name='bn7')(conv7)
    conv7 = ELU(name='elu7')(conv7)

    classes4 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes4')(conv4)
    classes5 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes5')(conv5)
    classes6 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes6')(conv6)
    classes7 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes7')(conv7)
    # Output shape of `boxes`: `(batch, height, width, n_boxes * 4)`
    boxes4 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes4')(conv4)
    boxes5 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes5')(conv5)
    boxes6 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes6')(conv6)
    boxes7 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes7')(conv7)

    anchors4 = AnchorBoxes(img_height, img_width, this_scale=scales[0], next_scale=scales[1],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[0], this_offsets=offsets[0],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors4')(boxes4)
    anchors5 = AnchorBoxes(img_height, img_width, this_scale=scales[1], next_scale=scales[2],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[1], this_offsets=offsets[1],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors5')(boxes5)
    anchors6 = AnchorBoxes(img_height, img_width, this_scale=scales[2], next_scale=scales[3],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[2], this_offsets=offsets[2],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors6')(boxes6)
    anchors7 = AnchorBoxes(img_height, img_width, this_scale=scales[3], next_scale=scales[4],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[3], this_offsets=offsets[3],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors7')(boxes7)

    classes4_reshaped = Reshape((-1, n_classes), name='classes4_reshape')(classes4)
    classes5_reshaped = Reshape((-1, n_classes), name='classes5_reshape')(classes5)
    classes6_reshaped = Reshape((-1, n_classes), name='classes6_reshape')(classes6)
    classes7_reshaped = Reshape((-1, n_classes), name='classes7_reshape')(classes7)
    boxes4_reshaped = Reshape((-1, 4), name='boxes4_reshape')(boxes4)
    boxes5_reshaped = Reshape((-1, 4), name='boxes5_reshape')(boxes5)
    boxes6_reshaped = Reshape((-1, 4), name='boxes6_reshape')(boxes6)
    boxes7_reshaped = Reshape((-1, 4), name='boxes7_reshape')(boxes7)
    anchors4_reshaped = Reshape((-1, 8), name='anchors4_reshape')(anchors4)
    anchors5_reshaped = Reshape((-1, 8), name='anchors5_reshape')(anchors5)
    anchors6_reshaped = Reshape((-1, 8), name='anchors6_reshape')(anchors6)
    anchors7_reshaped = Reshape((-1, 8), name='anchors7_reshape')(anchors7)
    classes_concat = Concatenate(axis=1, name='classes_concat')([classes4_reshaped,
                                                                 classes5_reshaped,
                                                                 classes6_reshaped,
                                                                 classes7_reshaped])
    boxes_concat = Concatenate(axis=1, name='boxes_concat')([boxes4_reshaped,
                                                             boxes5_reshaped,
                                                             boxes6_reshaped,
                                                             boxes7_reshaped])
    anchors_concat = Concatenate(axis=1, name='anchors_concat')([anchors4_reshaped,
                                                                 anchors5_reshaped,
                                                                 anchors6_reshaped,
                                                                 anchors7_reshaped])
    classes_softmax = Activation('softmax', name='classes_softmax')(classes_concat)
    predictions = Concatenate(axis=2, name='predictions')([classes_softmax, boxes_concat, anchors_concat])

    if mode == 'training':
        model = Model(inputs=x, outputs=predictions)
    elif mode == 'inference':
        decoded_predictions = DecodeDetections(confidence_thresh=confidence_thresh,
                                               iou_threshold=iou_threshold,
                                               top_k=top_k,
                                               nms_max_output_size=nms_max_output_size,
                                               coords=coords,
                                               normalize_coords=normalize_coords,
                                               img_height=img_height,
                                               img_width=img_width,
                                               name='decoded_predictions')(predictions)
        model = Model(inputs=x, outputs=decoded_predictions)

    if return_predictor_sizes:
        # The spatial dimensions are the same for the `classes` and `boxes` predictor layers.
        predictor_sizes = np.array([classes4._keras_shape[1:3],
                                    classes5._keras_shape[1:3],
                                    classes6._keras_shape[1:3],
                                    classes7._keras_shape[1:3]])
        return model, predictor_sizes
    else:
        return model


image_paths = glob.glob(r'D:\Video_Log\traffic/**/*rgb.png')
class_idx = {'left': 1, 'right': 2}


def read_annotation_xml(path):
    tree = ET.parse(path)
    root = tree.getroot()
    for p in root.findall('.//object'):
        id = class_idx[p.find('name').text]
        xmin = int(p.find('bndbox').find('xmin').text)
        ymin = int(p.find('bndbox').find('ymin').text)
        xmax = int(p.find('bndbox').find('xmax').text)
        ymax = int(p.find('bndbox').find('ymax').text)
        return id, xmin, ymin, xmax, ymax


def read_annotation_json(path):
    json_data = json.loads(open(path).read())
    shapes = json_data['shapes']
    points = shapes[0]['points']
    xmin, ymin = tuple(points[0])
    xmax, ymax = tuple(points[1])

    id = class_idx[shapes[0]['label']]
    return id, xmin, ymin, xmax, ymax


def generator(batch_size, ssd_input_encoder=None, im_h=300, im_w=300):
    batch_images = np.zeros((batch_size, 300, 300, 3))
    while True:
        annotations = []
        for i in range(batch_size):
            while True:
                image_name = random.choice(image_paths)
                if 'depth' not in image_name:
                    break
            mat = cv2.imread(image_name)
            h,w,c = mat.shape
            mat = cv2.resize(mat, (im_w, im_h))
            mat = mat / 255.
            # anno : label,xmin,ymin,xmax,ymax

            annotation_name = image_name.replace('.png', '.xml')
            if os.path.isfile(annotation_name):
                id, xmin, ymin, xmax, ymax = read_annotation_xml(annotation_name)
            else:
                id, xmin, ymin, xmax, ymax = read_annotation_json(annotation_name.replace('xml', 'json'))
            # id left 1, right 2
            if bool(random.getrandbits(1)):
                id = 3 - id
                xmax,xmin = w - xmin, w - xmax
                mat = cv2.flip(mat, 0)

            annotations.append(np.array([[id, xmin, ymin, xmax, ymax]]))
            batch_images[i] = mat
        if ssd_input_encoder is not None:
            batch_annotions = ssd_input_encoder(annotations)
        else:
            batch_annotions = annotations
        yield batch_images, batch_annotions


class My_Callback(Callback):

    def on_epoch_end(self, epoch, logs={}):
        # while True:
        image_name = random.choice(image_paths)
            # if 'depth' not in image_name:
            #     break

        image = cv2.imread(image_name)

        h, w, c = image.shape
        resized = cv2.resize(image, (300, 300)) / 255.
        y = self.model.predict(np.array([resized]))

        y_pred_decoded = decode_detections(y,
                                           confidence_thresh=0.5,
                                           iou_threshold=0.45,
                                           top_k=200,
                                           normalize_coords=False,
                                           img_height=h,
                                           img_width=w)
        if len(y_pred_decoded[0] > 0):
            line = y_pred_decoded[0][0]

            # print(image.shape,y[0][0].shape)
            if line[1] > 0:
                label = 'None'
                if line[0] == 1:
                    label = 'left'
                elif line[0] == 2:
                    label = 'right'

                cv2.putText(image, label, (int(line[2]), int(line[3])), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2,
                            (0, 255, 255), 2, cv2.LINE_AA)
                cv2.rectangle(image, (int(line[2]), int(line[3])), (int(line[4]), int(line[5])), (0, 255, 0), 1)
                cv2.imwrite('log/{}.jpg'.format(epoch), image)

            np.set_printoptions(precision=2, suppress=True, linewidth=90)
        self.model.save_weights('weights/traffic_7.h5')

        print("Predicted boxes:\n")
        print('   class   conf xmin   ymin   xmax   ymax')
        print(y_pred_decoded[0])
        return


if __name__ == '__main__':
    model = build_model()
    model.summary()
    callback = My_Callback()
    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

    pun_loss = PunLoss(neg_pos_ratio=3, alpha=1.0)
    predictor_sizes = [model.get_layer('classes4').output_shape[1:3],
                       model.get_layer('classes5').output_shape[1:3],
                       model.get_layer('classes6').output_shape[1:3],
                       model.get_layer('classes7').output_shape[1:3]]
    model.compile(optimizer=adam, loss=pun_loss.compute_context_loss)

    img_height = 300  # Height of the input images
    img_width = 300  # Width of the input images
    img_channels = 3  # Number of color channels of the input images
    intensity_mean = 127.5  # Set this to your preference (maybe `None`). The current settings transform the input pixel values to the interval `[-1,1]`.
    intensity_range = 127.5  # Set this to your preference (maybe `None`). The current settings transform the input pixel values to the interval `[-1,1]`.
    n_classes = 2  # Number of positive classes
    scales = [0.08, 0.16, 0.32, 0.64,
              0.96]  # An explicit list of anchor box scaling factors. If this is passed, it will override `min_scale` and `max_scale`.
    aspect_ratios = [0.5, 1.0, 2.0]  # The list of aspect ratios for the anchor boxes
    two_boxes_for_ar1 = True  # Whether or not you want to generate two anchor boxes for aspect ratio 1
    steps = None  # In case you'd like to set the step sizes for the anchor box grids manually; not recommended
    offsets = None
    clip_boxes = False
    variances = [1.0, 1.0, 1.0, 1.0]
    normalize_coords = True

    encoder = SSDInputEncoder(img_height=img_height,
                              img_width=img_width,
                              n_classes=2,
                              predictor_sizes=predictor_sizes,
                              scales=scales,
                              aspect_ratios_global=aspect_ratios,
                              two_boxes_for_ar1=two_boxes_for_ar1,
                              steps=steps,
                              offsets=offsets,
                              clip_boxes=clip_boxes,
                              variances=variances,
                              matching_type='multi',
                              pos_iou_threshold=0.5,
                              neg_iou_limit=0.3,
                              normalize_coords=normalize_coords

                              )

    model.load_weights('weights/traffic_7.h5')
    model.fit_generator(generator(32, encoder), steps_per_epoch=100, epochs=20, callbacks=[callback])
