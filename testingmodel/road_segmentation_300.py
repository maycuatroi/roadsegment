import glob
import random

import cv2
import numpy as np
from keras.callbacks import Callback
from keras.layers import Dense, Flatten, Input, Concatenate, Permute, \
    AveragePooling2D, Conv2D, Dropout, MaxPooling2D, UpSampling2D
from keras.models import Model

data_source = r'F:\Video_Log/'
label_paths = glob.glob(data_source+'labels/*.png')


def load_segment(label_name):
    segment= cv2.imread(label_name,0)

    return segment


def generator(batch_size):
    batch_features = np.zeros((batch_size, 300, 300,3))
    batch_segment = np.zeros((batch_size,2,300, 300))
    while True:
        for i in range(batch_size):
            # choose random index in features
            # index = random.choice(len(image_paths), 1)
            label_name = random.choice(label_paths)
            image_name = label_name.replace('labels','images')
            if '-' in image_name:
                image_name = image_name.replace('png','jpg')
            mat = cv2.imread(image_name)
            mat = cv2.resize(mat,(300,300))
            # croped = mat[84:240, :]
            # resized = cv2.resize(croped, (84, 84))
            # gray = cv2.cvtColor(resized, cv2.COLOR_RGB2GRAY)
            segment = load_segment(label_name)
            segment =  cv2.resize(segment,(300,300))
            road = np.zeros((300,300))
            line = np.zeros((300,300))
            road[segment==2]=1
            line[segment==1]=1
            # segment[segment==2] = 0


            batch_features[i] = mat / 255.
            batch_segment[i] = np.array([road,line])
        yield batch_features, batch_segment

# while True:
#     bath = next(generator(1))
#
#     cv2.imshow('image',bath[0][0])
#     cv2.imshow('segment',bath[1][0])
#     cv2.waitKey(0)

class My_Callback(Callback):

    def on_epoch_end(self, epoch, logs={}):
        bath = next(generator(1))
        image = bath[0][0]

        y = self.model.predict(np.array([image]))
        image *= 255.
        # y *= 255.
        road = y[0][0]
        line = y[0][1]

        image = cv2.resize(image,(300,300))
        segment = np.zeros((300,300,3))
        segment[road>0.5]=(0,255,0)
        segment[line>0.5]=(0,255,255)
        cv2.imwrite('log/{}.jpg'.format(epoch), np.concatenate((image,segment),1))
        self.model.save_weights('weights/pun_net_backup_300.h5')
        return

def fucked_fake_model():
    image_input = Input((300, 300,3))
    # crop = Cropping2D(cropping=((0, 84), (0, 0)), data_format='channels_last')(image_input)

    x = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv1')(image_input)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv2')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv2')(x)
    x = AveragePooling2D(pool_size=(2, 2))(x)
    x= Flatten()(x)
    x = Dense(128,activation='relu')(x)
    x = Dense(1,activation='sigmoid')(x)
    model = Model(input=image_input, output=x)
    model.compile(loss="binary_crossentropy", optimizer='adam', metrics=['accuracy'])
    return model

def pun_net():
    image_input = Input((300, 300,3))
    # crop = Cropping2D(cropping=((0, 84), (0, 0)), data_format='channels_last')(image_input)

    conv1 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv1')(image_input)
    conv1 = Dropout(0.2)(conv1)
    conv1 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv2')(conv1)

    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv3')(pool1)
    conv2 = Dropout(0.2)(conv2)
    conv2 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv4')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(128, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv5')(pool2)
    conv3 = Dropout(0.2)(conv3)
    conv3 = Conv2D(128, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv6')(conv3)

    up1 = Concatenate()([UpSampling2D(size=(2, 2))(conv3), conv2])
    conv4 = Conv2D(84, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv7')(up1)
    conv4 = Dropout(0.2)(conv4)
    conv4 = Conv2D(84, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv8')(conv4)

    up2 = Concatenate()([UpSampling2D(size=(2, 2))(conv4), conv1])
    conv5 = Conv2D(62, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv9')(up2)
    conv5 = Dropout(0.2)(conv5)
    x = Conv2D(62, (3, 3), strides=1, activation='relu', padding='same',name='seg_conv10')(conv5)

    conv6 = Conv2D(2, 1, strides=1, activation='sigmoid', padding='same',name='seg_conv11')(x)

    outputs = Permute((3, 1, 2))(conv6)
    model = Model(input=image_input, output=outputs)

    model.compile(loss="binary_crossentropy", optimizer='sgd', metrics=['accuracy'])

    return model






model = pun_net()
model.summary()
model.load_weights('weights/pun_net_backup_300.h5')
callback = My_Callback()
# model.fit_generator(generator(5), steps_per_epoch=500, epochs=5, callbacks=[callback])
# model.save_weights('weights/pun_net_backup_100.h5')

out_vid = cv2.VideoWriter('output_segment.avi',cv2.VideoWriter_fourcc(*'XVID'), 24.0, (800,400))
video_path =r"C:\Users\Binh Bum\Downloads\data 20190226\data 20190226\1551173117.62_rgb.avi"
cap = cv2.VideoCapture(video_path)
writeout = False
while True:
    # image = next(generator(1))[0][0]
    ret,mat = cap.read()
    if not ret:
        cap = cv2.VideoCapture(video_path)
        continue
        # break
    image  = cv2.resize(mat,(300,300))/255.
    y = model.predict(np.array([image]))

    # image *= 255.
    # y *= 255.
    road = y[0][0]
    line = y[0][1]
    # segment[segment!=0] = 255.
    # print(image.shape,y[0][0].shape)
    image = cv2.resize(image,(300,300))
    segment = np.zeros((300,300,3))
    # road =  cv2.cvtColor( road,cv2.COLOR_GRAY2RGB)
    # line =  cv2.cvtColor( line,cv2.COLOR_GRAY2RGB)
    segment[road>0.5]=(0,255,0)
    segment[line>0.5]=(0,255,255)
    segment = segment.astype(np.uint8)
    # image = np.concatenate((image,road),1)
    image = (image * 255.).astype(np.uint8)
    mat = cv2.resize(mat,(300,300))
    out_image = np.concatenate((mat,segment),1)
    cv2.imshow('test',out_image )
    k=cv2.waitKey(100)
    if k ==ord('s'):
        writeout = True
    if writeout:
        out_vid.write(out_image )

out_vid.release()
