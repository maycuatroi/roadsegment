import tensorflow as tf

# c = tf.constant("Hello, distributed TensorFlow!")
cluster = tf.train.ClusterSpec({"ps": ["localhost:8888"],
                                "worker": ["localhost:8887"]})

tf.train.MonitoredTrainingSession()
server = tf.train.Server(cluster,
                         job_name='ps',
                         task_index=0)
# server = tf.train.Server.create_local_server()
print(server.target)
sess = tf.Session(server.target)
server.join()