import glob
import random

import cv2
import numpy as np
from keras.callbacks import Callback
from keras.engine import Layer
from keras.layers import Dense, Flatten, Input, Concatenate, Permute, \
    AveragePooling2D, Conv2D, Dropout, MaxPooling2D, UpSampling2D, BatchNormalization, ELU, Reshape, Activation
from keras.models import Model
from imgaug import augmenters as iaa
import imgaug as ia
import threading
import time
import tensorflow as tf
from keras.regularizers import l2
import keras.backend as K
from ssd_ultis.keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from ssd_ultis.keras_layers.keras_layer_DecodeDetections import DecodeDetections


class ThreadingBatches(object):

    def __init__(self, generator=None, fast_generator=None):
        self.generator = generator
        self.stack = []
        self.fast_generator = fast_generator
        self.queue_size = 20
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True  # Daemonize thread
        thread.start()  # Start the execution

    def run(self):
        while True:
            if (len(self.stack) < self.queue_size):
                try:
                    batch = next(self.generator)
                    self.stack.append(batch)
                except ValueError:
                    None

    def popNextBatch(self):
        if len(self.stack) == 0:
            time.sleep(0.1)
            return next(self.fast_generator)

        return self.stack.pop(0)


data_source = r'D:\Video_Log/'
label_paths = glob.glob(data_source + 'labels/*.png')


def image_func(img):
    # img = img*255.
    # cv2.imwrite('test.png',img)
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # img = cv2.resize(img, (300, 300))
    # return img.astype('float32')/255.
    img = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
    # input(img)
    gray = (0,0,0)
    blue = (200,0,0)
    white = (250,250,250)
    image = np.zeros((300, 300,3))
    image[img < 0.8] = white
    image[img >= 0.8] = blue
    image[img <= 0.5] = gray
    image = image / 255.
    # print(img.mean(),image.mean())
    return image.astype('float32')


def image_tensor_func(img4d):
    results = []
    for img3d in img4d:
        rimg3d = image_func(img3d)
        results.append(np.expand_dims(rimg3d, axis=0))
        # print('call')
    return np.concatenate(results, axis=0)


def py_func(func, inp, Tout, stateful=True, name=None, grad=None):
    rnd_name = 'PyFuncGrad' + str(np.random.randint(0, 1E+8))
    tf.RegisterGradient(rnd_name)(grad)
    g = tf.get_default_graph()
    with g.gradient_override_map({"PyFunc": rnd_name}):
        return tf.py_func(func, inp, Tout, stateful=stateful, name=name)

class CustomLayer(Layer):

    def build(self, input_shape):
        print('call Build')
        super().build(input_shape)

    def _MySquareGrad(self,op, grad):
        x = op.inputs[0]
        return grad

    def call(self, xin):
        xout = py_func(image_tensor_func,
                          [xin],
                          'float32',
                          stateful=True,
                          name='cvOpt',grad=self._MySquareGrad)

        # xout = K.stop_gradient(xout)
        xout.set_shape([xin.shape[0], 2,300,300])
        self.trainable = False
        return xout

    def compute_output_shape(self, sin):
        return (sin[0], 2,300,300)


def load_segment(label_name):
    segment = cv2.imread(label_name, 0)
    return segment


seq = iaa.Sequential([
    iaa.Crop(px=(0, 3)),  # crop images from each side by 0 to 16px (randomly chosen)
    iaa.Fliplr(0.5),  # horizontally flip 50% of the images
    iaa.PerspectiveTransform(0.04),
    iaa.PiecewiseAffine(0.04),
    iaa.Snowflakes((0.05, 0.25)),
    # iaa.Affine(rotate=(-45, 45))
])


def generator(batch_size):
    batch_features = np.zeros((batch_size, 100, 100, 3))
    batch_segment = np.zeros((batch_size, 2, 300, 300))
    while True:
        seq_det = seq.to_deterministic()
        mats = []
        segs = []

        for i in range(batch_size):
            label_name = random.choice(label_paths)
            image_name = label_name.replace('labels', 'images')
            if '-' in image_name:
                image_name = image_name.replace('png', 'jpg')
            mat = cv2.imread(image_name)
            mats.append(mat)
            segment = load_segment(label_name)
            segment = cv2.resize(segment, (300, 300))
            segmap = ia.SegmentationMapOnImage(segment, shape=segment.shape, nb_classes=3)
            segs.append(segmap)

        mats = seq_det.augment_images(np.array(mats))
        segs = seq_det.augment_segmentation_maps(np.array(segs))

        for i in range(len(mats)):
            segment = segs[i].arr
            road, line, bg = cv2.split(np.round(segment))
            road = 1 - road
            mat = mats[i]
            mat = cv2.resize(mat, (100, 100))
            batch_features[i] = mat
            batch_segment[i] = np.array([road, line])
        yield batch_features / 255., batch_segment


def generator_no_augment(batch_size):
    batch_features = np.zeros((batch_size, 100, 100, 3))
    batch_segment = np.zeros((batch_size, 2, 300, 300))
    while True:
        for i in range(batch_size):
            label_name = random.choice(label_paths)
            image_name = label_name.replace('labels', 'images')
            if '-' in image_name:
                image_name = image_name.replace('png', 'jpg')
            mat = cv2.imread(image_name)
            mat = cv2.resize(mat, (100, 100))
            segment = load_segment(label_name)
            segment = cv2.resize(segment, (300, 300))
            road = np.zeros((300, 300))
            line = np.zeros((300, 300))
            road[segment == 2] = 1
            line[segment == 1] = 1
            batch_features[i] = mat / 255.
            batch_segment[i] = np.array([road, line])
        yield batch_features, batch_segment


# thread_train = ThreadingBatches(generator(5), generator_no_augment(5))


def thread_generator():
    while True:
        yield thread_train.popNextBatch()


# while True:
#     bath = next(generator(5))
#     segment =  np.zeros((400,400,3))
#     road_mat = bath[1][0][0]
#     line_mat = bath[1][0][1]
#     segment[road_mat==1]=(255,0,0)
#     segment[line_mat==1]=(0,255,0)
#     image = cv2.resize(bath[0][0],(400,400))
#     cv2.imshow('image',np.concatenate((image,segment),1))
#     # cv2.imshow('road',segment)
#     cv2.waitKey(0)

class My_Callback(Callback):

    def on_epoch_end(self, epoch, logs={}):
        bath = next(generator(1))
        image = bath[0][0]

        y = self.model.predict(np.array([image]))
        image *= 255.
        # y *= 255.
        road = y[0][0]
        line = y[0][1]

        image = cv2.resize(image, (400, 400))
        segment = np.zeros((400, 400, 3))
        segment[road > 0.5] = (0, 255, 0)
        segment[line > 0.5] = (0, 255, 255)
        cv2.imwrite('log/{}.jpg'.format(epoch), np.concatenate((image, segment), 1))
        self.model.save_weights('weights/test_aug_pun_net_backup_100.h5')
        return


def build_ssd_model(image_size=(300, 300, 3),
                    n_classes=2,
                    mode='training',
                    l2_regularization=0.0,
                    scales=[0.08, 0.16, 0.32, 0.64, 0.96],
                    two_boxes_for_ar1=True,
                    clip_boxes=False,
                    variances=[1.0, 1.0, 1.0, 1.0],
                    coords='centroids',
                    normalize_coords=False,
                    confidence_thresh=0.01,
                    iou_threshold=0.45,
                    top_k=200,
                    nms_max_output_size=400,
                    return_predictor_sizes=False):
    number_predictor = 4
    steps = [None] * number_predictor
    offsets = [None] * number_predictor
    n_classes += 1
    l2_reg = l2_regularization
    img_height, img_width, img_channels = image_size[0], image_size[1], image_size[2]

    x = Input(shape=(img_height, img_width, img_channels))
    conv1 = Conv2D(32, (5, 5), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv1')(x)
    conv1 = BatchNormalization(axis=3, momentum=0.99, name='bn1')(
        conv1)
    conv1 = ELU(name='elu1')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2), name='pool1')(conv1)

    conv2 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv2')(pool1)
    conv2 = BatchNormalization(axis=3, momentum=0.99, name='bn2')(conv2)
    conv2 = ELU(name='elu2')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2), name='pool2')(conv2)

    conv3 = Conv2D(64, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv3')(pool2)
    conv3 = BatchNormalization(axis=3, momentum=0.99, name='bn3')(conv3)
    conv3 = ELU(name='elu3')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2), name='pool3')(conv3)

    conv4 = Conv2D(64, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv4')(pool3)
    conv4 = BatchNormalization(axis=3, momentum=0.99, name='bn4')(conv4)
    conv4 = ELU(name='elu4')(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2), name='pool4')(conv4)

    conv5 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv5')(pool4)
    conv5 = BatchNormalization(axis=3, momentum=0.99, name='bn5')(conv5)
    conv5 = ELU(name='elu5')(conv5)
    pool5 = MaxPooling2D(pool_size=(2, 2), name='pool5')(conv5)

    conv6 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv6')(pool5)
    conv6 = BatchNormalization(axis=3, momentum=0.99, name='bn6')(conv6)
    conv6 = ELU(name='elu6')(conv6)
    pool6 = MaxPooling2D(pool_size=(2, 2), name='pool6')(conv6)

    conv7 = Conv2D(32, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv7')(pool6)
    conv7 = BatchNormalization(axis=3, momentum=0.99, name='bn7')(conv7)
    conv7 = ELU(name='elu7')(conv7)

    classes4 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes4')(conv4)
    classes5 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes5')(conv5)
    classes6 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes6')(conv6)
    classes7 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes7')(conv7)
    # Output shape of `boxes`: `(batch, height, width, n_boxes * 4)`
    boxes4 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes4')(conv4)
    boxes5 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes5')(conv5)
    boxes6 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes6')(conv6)
    boxes7 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes7')(conv7)

    anchors4 = AnchorBoxes(img_height, img_width, this_scale=scales[0], next_scale=scales[1],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[0], this_offsets=offsets[0],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors4')(boxes4)
    anchors5 = AnchorBoxes(img_height, img_width, this_scale=scales[1], next_scale=scales[2],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[1], this_offsets=offsets[1],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors5')(boxes5)
    anchors6 = AnchorBoxes(img_height, img_width, this_scale=scales[2], next_scale=scales[3],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[2], this_offsets=offsets[2],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors6')(boxes6)
    anchors7 = AnchorBoxes(img_height, img_width, this_scale=scales[3], next_scale=scales[4],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[3], this_offsets=offsets[3],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors7')(boxes7)

    classes4_reshaped = Reshape((-1, n_classes), name='classes4_reshape')(classes4)
    classes5_reshaped = Reshape((-1, n_classes), name='classes5_reshape')(classes5)
    classes6_reshaped = Reshape((-1, n_classes), name='classes6_reshape')(classes6)
    classes7_reshaped = Reshape((-1, n_classes), name='classes7_reshape')(classes7)
    boxes4_reshaped = Reshape((-1, 4), name='boxes4_reshape')(boxes4)
    boxes5_reshaped = Reshape((-1, 4), name='boxes5_reshape')(boxes5)
    boxes6_reshaped = Reshape((-1, 4), name='boxes6_reshape')(boxes6)
    boxes7_reshaped = Reshape((-1, 4), name='boxes7_reshape')(boxes7)
    anchors4_reshaped = Reshape((-1, 8), name='anchors4_reshape')(anchors4)
    anchors5_reshaped = Reshape((-1, 8), name='anchors5_reshape')(anchors5)
    anchors6_reshaped = Reshape((-1, 8), name='anchors6_reshape')(anchors6)
    anchors7_reshaped = Reshape((-1, 8), name='anchors7_reshape')(anchors7)
    classes_concat = Concatenate(axis=1, name='classes_concat')([classes4_reshaped,
                                                                 classes5_reshaped,
                                                                 classes6_reshaped,
                                                                 classes7_reshaped])
    boxes_concat = Concatenate(axis=1, name='boxes_concat')([boxes4_reshaped,
                                                             boxes5_reshaped,
                                                             boxes6_reshaped,
                                                             boxes7_reshaped])
    anchors_concat = Concatenate(axis=1, name='anchors_concat')([anchors4_reshaped,
                                                                 anchors5_reshaped,
                                                                 anchors6_reshaped,
                                                                 anchors7_reshaped])
    classes_softmax = Activation('softmax', name='classes_softmax')(classes_concat)
    predictions = Concatenate(axis=2, name='predictions')([classes_softmax, boxes_concat, anchors_concat])

    if mode == 'training':
        model = Model(inputs=x, outputs=predictions)
    elif mode == 'inference':
        decoded_predictions = DecodeDetections(confidence_thresh=confidence_thresh,
                                               iou_threshold=iou_threshold,
                                               top_k=top_k,
                                               nms_max_output_size=nms_max_output_size,
                                               coords=coords,
                                               normalize_coords=normalize_coords,
                                               img_height=img_height,
                                               img_width=img_width,
                                               name='decoded_predictions')(predictions)
        model = Model(inputs=x, outputs=decoded_predictions)

    if return_predictor_sizes:
        # The spatial dimensions are the same for the `classes` and `boxes` predictor layers.
        predictor_sizes = np.array([classes4._keras_shape[1:3],
                                    classes5._keras_shape[1:3],
                                    classes6._keras_shape[1:3],
                                    classes7._keras_shape[1:3]])
        return model, predictor_sizes
    else:
        return model


def image_func(img):
    img = cv2.resize(img, (100, 100))
    return img.astype('float32')


def image_tensor_func(img4d):
    results = []
    for img3d in img4d:
        rimg3d = image_func(img3d)
        results.append(np.expand_dims(rimg3d, axis=0))
    return np.concatenate(results, axis=0)


class Custom_resize_Layer(Layer):
    def call(self, xin):
        xout = tf.py_func(image_tensor_func,
                          [xin],
                          'float32',
                          stateful=False,
                          name='cvOpt')
        xout = K.stop_gradient(xout)  # explicitly set no grad
        xout.set_shape([xin.shape[0], 300, 300, xin.shape[-1]])  # explicitly set output shape
        return xout

    def compute_output_shape(self, sin):
        return (sin[0], 300, 300, sin[-1])


def pun_net():
    road_input = Input((100, 100, 3))
    conv1 = Conv2D(32, (3, 3), strides=2, activation='relu', padding='same', name='seg_conv1')(road_input)
    x = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv2')(conv1)
    conv2 = Conv2D(64, (3, 3), strides=2, activation='relu', padding='same', name='seg_conv3')(x)
    x = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv4')(conv2)
    conv3 = Conv2D(128, (3, 3), strides=2, activation='relu', padding='same', name='seg_conv5')(x)
    x = Conv2D(16, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv6')(conv3)
    x = Flatten()(x)

    road_out = Dense(5 * 5, activation='sigmoid')(x)
    line_out = Dense(5 * 5, activation='sigmoid')(x)
    x = Concatenate()([line_out,road_out])
    x = CustomLayer(name='custom')(x)

    model = Model(inputs=road_input, outputs=x)
    model.compile(loss="binary_crossentropy", optimizer='sgd', metrics=['accuracy'])

    return model


# def pun_net_reconstruction():


model = pun_net()
model.summary()
# model.load_weights('weights/test_aug_pun_net_backup_100.h5', by_name=True)
callback = My_Callback()

# model.fit_generator(thread_generator(), steps_per_epoch=500, epochs=15, callbacks=[callback])
model.fit_generator(generator_no_augment(5), steps_per_epoch=500, epochs=15, callbacks=[callback])
# model.save_weights('weights/pun_net_backup_100.h5')

video_path = r"D:\video\1551349372.26_rgb.avi"
cap = cv2.VideoCapture(video_path)

while True:
    # image = next(generator(1))[0][0]
    ret, mat = cap.read()
    if not ret:
        cap = cv2.VideoCapture(video_path)
        continue
        # break
    sign_image = cv2.resize(mat, (300, 300)) / 255.
    road_image = cv2.resize(mat, (100, 100)) / 255.
    # y = model.predict([np.array([road_image]),np.array([sign_image])])
    y = model.predict(np.array([road_image]))
    model.save('weights/punnet_extend.h5')
    # y_seg = y[0]
    # y_sign = y[1]

    # y= y_seg
    road = y[0][0]
    line = y[0][1]
    image = cv2.resize(mat, (400, 400))
    segment = np.zeros((400, 400, 3))
    segment[road > 0.5] = (0, 255, 0)
    segment[line > 0.5] = (0, 255, 255)
    segment = segment.astype(np.uint8)
    # image = np.concatenate((image,road),1)
    image = (image * 255.).astype(np.uint8)
    mat = cv2.resize(mat, (400, 400))
    out_image = np.concatenate((mat, segment), 1)
    cv2.imshow('test', out_image)
    k = cv2.waitKey(1)
    # if k ==ord('s'):
    #     writeout = True
    if k == 32:
        cv2.waitKey(0)
