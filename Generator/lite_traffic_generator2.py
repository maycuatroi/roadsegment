import glob
import cv2
import numpy as np
import glob
import json
import os
import random
import numpy as np
import cv2
import xml.etree.ElementTree as ET


class TrafficLiteGenerator:
    def __init__(self, image_path):
        self.image_paths = glob.glob(r'{}/**/*.png'.format(image_path))
        self.class_idx = {'bg': 0, 'left': 1, 'right': 2}

    def generator(self, batch_size):
        batch_images = np.zeros((batch_size, 32, 32, 3))
        # None - left - right
        batch_annotions = np.zeros((batch_size, 3))
        while True:
            for i in range(batch_size):
                image_name = random.choice(self.image_paths)
                label = image_name.split(os.sep)[-2]
                label_id = self.class_idx[label]
                mat = cv2.imread(image_name)
                mat = cv2.resize(mat,(32,32))
                if bool(random.getrandbits(1)):  # random flip
                    mat = cv2.flip(mat, 1)
                    if label_id == 1:
                        label_id = 2
                    elif label_id == 2:
                        label_id = 1


                batch_images[i] = mat
                batch_annotions[i] = batch_annotions[i] * 0
                batch_annotions[i][label_id] = 1.

            yield batch_images/255., batch_annotions


if __name__ == '__main__':
    # image_paths = glob.glob(r'D:\Video_Log\traffic\right/*rgb.png')
    while True:
        generator = TrafficLiteGenerator(r'D:\Video_Log\32x32')
        batch = next(generator.generator(10))
        image = None
        for mat in batch[0]:

            if image is None:
                image = mat
            else:
                image = np.concatenate((image, mat))

        print(batch[1])
        # hsv = cv2.cvtColor(mat,cv2.COLOR_RGB2HSV)
        # mask = cv2.inRange(hsv, (9, 124, 100), (16, 250, 150))
        cv2.imshow('test', image)

        cv2.waitKey(0)
