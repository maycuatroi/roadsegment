import glob
import random
import tensorflow as tf
import cv2
import numpy as np
from keras.callbacks import Callback
from keras.engine import Layer
from keras.layers import Dense, Flatten, Input, Concatenate, Permute, \
    AveragePooling2D, Conv2D, Dropout, MaxPooling2D, UpSampling2D, Lambda
from keras.models import Model
import keras.backend as K

from CallBacks.Log_callback import Log_Callback

image_paths = glob.glob(r'F:\Video_Log\unlabeled\*.jpg')


def generator(batch_size):
    batch_features = np.zeros((batch_size, 300, 300, 3))
    batch_segment = np.zeros((batch_size, 300, 300,3))
    while True:
        for i in range(batch_size):
            # choose random index in features
            # index = random.choice(len(image_paths), 1)
            # label_name = ''
            # while 'rgb' not in label_name:
            label_name = random.choice(image_paths)

            mat = cv2.imread(label_name)
            mat = mat[100:, :]

            mat = cv2.resize(mat, (300, 300))

            batch_features[i] = mat / 255.

            batch_segment[i] =mat / 255.
        yield batch_features, batch_segment

def image_func(img):
    # img = img*255.
    # cv2.imwrite('test.png',img)
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # img = cv2.resize(img, (300, 300))
    # return img.astype('float32')/255.
    img = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
    # input(img)
    gray = (0,0,0)
    blue = (200,0,0)
    white = (250,250,250)
    image = np.zeros((300, 300,3))
    image[img < 0.8] = white
    image[img >= 0.8] = blue
    image[img <= 0.5] = gray
    image = image / 255.
    # print(img.mean(),image.mean())
    return image.astype('float32')


def image_tensor_func(img4d):
    results = []
    for img3d in img4d:
        rimg3d = image_func(img3d)
        results.append(np.expand_dims(rimg3d, axis=0))
        # print('call')
    return np.concatenate(results, axis=0)


def py_func(func, inp, Tout, stateful=True, name=None, grad=None):

    # Need to generate a unique name to avoid duplicates:
    rnd_name = 'PyFuncGrad' + str(np.random.randint(0, 1E+8))

    tf.RegisterGradient(rnd_name)(grad)  # see _MySquareGrad for grad example
    g = tf.get_default_graph()
    with g.gradient_override_map({"PyFunc": rnd_name}):
        return tf.py_func(func, inp, Tout, stateful=stateful, name=name)

class CustomLayer(Layer):

    def build(self, input_shape):
        print('call Build')
        # self.kernel = self.add_weight(name='kernel',
        #                               shape=(300,300,3),
        #                               initializer='uniform',
        #                               trainable=False)
        super().build(input_shape)
    def _MySquareGrad(self,op, grad):
        # x = op.inputs[0]
        return grad #* 20 * x  # add a "small" error just to see the difference:

    def call(self, xin):
        xout = py_func(image_tensor_func,
                          [xin],
                          'float32',
                          stateful=True,
                          name='cvOpt',grad=self._MySquareGrad)
        # xout = K.stop_gradient(xout)

        # gradients = K.gradients(0,xout)
        xout.set_shape([xin.shape[0], 300, 300, 3])
        return xout
        # return K.dot(xout,self.kernel)


    def compute_output_shape(self, sin):
        return (sin[0], 300, 300, 3)
def convert_image_to_cells(x):
    # t = (0.21 * x[:,:,:,:1]) + (0.72 * x[:,:,:,1:2]) + (0.07 * x[:,:,:,-1:])
    a = tf.identity(x)


    a[x < 0.7] = 0.5
    a[x < 0.3] = 0.2
    a[x >= 0.7] = 0.8

    return a
def to_pow(x):
    b = x*tf.cast(x>1, 'float32') + tf.cast(x<=1, 'float32')
    return b


def pun_net():
    image_input = Input((300, 300, 3))
    # crop = Cropping2D(cropping=((0, 84), (0, 0)), data_format='channels_last')(image_input)

    conv1 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv1')(image_input)
    conv1 = Dropout(0.2)(conv1)
    conv1 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv2')(conv1)

    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv3')(pool1)
    conv2 = Dropout(0.2)(conv2)
    conv2 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv4')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(128, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv5')(pool2)
    conv3 = Dropout(0.2)(conv3)
    conv3 = Conv2D(128, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv6')(conv3)

    up1 = Concatenate()([UpSampling2D(size=(2, 2))(conv3), conv2])
    conv4 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv7')(up1)
    conv4 = Dropout(0.2)(conv4)
    conv4 = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv8')(conv4)

    up2 = Concatenate()([UpSampling2D(size=(2, 2))(conv4), conv1])
    conv5 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv9')(up2)
    conv5 = Dropout(0.2)(conv5)
    conv5 = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv10')(conv5)

    x = Conv2D(3, 1, strides=1, activation='sigmoid', padding='same')(conv5)


    x = CustomLayer(name='custom')(x)
    # x=Lambda(convert_image_to_cells)(x)

    # x = Permute((3, 1, 2))(x)
    model = Model(input=image_input, output=x)

    model.compile(loss="mse", optimizer='sgd', metrics=['accuracy'])

    return model


model = pun_net()
model.summary()
# model.load_weights('../weights/road_segment_autoencoder.h5')
callback = Log_Callback(generator, log_path='../log', weights_path='../weights/unsupervided.h5')

model.fit_generator(generator(5), steps_per_epoch=100, epochs=100, callbacks=[callback])
