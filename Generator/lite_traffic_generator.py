import glob

import cv2
import numpy as np
import glob
import json
import os
import random
import numpy as np
import cv2
import xml.etree.ElementTree as ET

class TrafficLiteGenerator:
    def __init__(self, image_path,false_condition_path):
        self.image_false_paths = glob.glob(r'{}/*.png'.format(false_condition_path))
        self.image_paths = glob.glob(r'{}/**/*rgb.png'.format(image_path))
        self.class_idx = {'left': 1, 'right': 2}

    def read_annotation_xml(self, path):
        tree = ET.parse(path)
        root = tree.getroot()
        for p in root.findall('.//object'):
            id = self.class_idx[p.find('name').text]
            xmin = int(p.find('bndbox').find('xmin').text)
            ymin = int(p.find('bndbox').find('ymin').text)
            xmax = int(p.find('bndbox').find('xmax').text)
            ymax = int(p.find('bndbox').find('ymax').text)
            return id, xmin, ymin, xmax, ymax

    def read_annotation_json(self, path):
        json_data = json.loads(open(path).read())
        shapes = json_data['shapes']
        points = shapes[0]['points']
        x1, y1 = tuple(points[0])
        x2, y2 = tuple(points[1])
        xs = [x1, x2]
        ys = [y1, y2]
        xmin, ymin, xmax, ymax = min(xs), min(ys), max(xs), max(ys)

        id = self.class_idx[shapes[0]['label']]
        return id, xmin, ymin, xmax, ymax

    def generator(self, batch_size, mode='train', im_h=32, im_w=32):
        batch_images = np.zeros((batch_size, 32, 32, 3))
        #None - left - right
        batch_annotions = np.zeros((batch_size, 3))
        while True:
            for i in range(batch_size):
                if bool(random.getrandbits(1)):  # gen false annotation
                    image_name = random.choice(self.image_paths)
                    mat = cv2.imread(image_name)
                    h, w, c = mat.shape
                    annotation_name = image_name.replace('.png', '.xml')
                    if os.path.isfile(annotation_name):
                            id, xmin, ymin, xmax, ymax = self.read_annotation_xml(annotation_name)
                    elif os.path.isfile(annotation_name.replace('xml', 'json')):
                            id, xmin, ymin, xmax, ymax = self.read_annotation_json(annotation_name.replace('xml', 'json'))

                else:
                    id = 0
                    mat=  cv2.imread(random.choice(self.image_false_paths))
                    h,w,c= mat.shape
                    xmax = random.randint(32,w)
                    xmin =  max(xmax-32,0)
                    ymax = random.randint(32,h)
                    ymin = max(ymax-32,0)


                if bool(random.getrandbits(1)):
                    if id == 1 :
                        id = 2
                    elif id ==2:
                        id = 1
                    xmax, xmin = w - xmin, w - xmax
                    mat = cv2.flip(mat, 0)

                croped = mat[ymin:ymax,xmin:xmax]
                croped = cv2.resize(croped,(32,32))/255.
                batch_annotions[i] = batch_annotions[i]*0
                batch_annotions[i][id] = 1
                batch_images[i] = croped

            yield batch_images, batch_annotions
# if __name__ == '__main__':
#     # image_paths = glob.glob(r'D:\Video_Log\traffic\right/*rgb.png')
#     while True:
#         generator = TrafficLiteGenerator(r'D:\Video_Log\traffic')
#         batch = next(generator.generator(10))
#         image = None
#         for mat in batch[0]:
#
#             if image is None:
#                 image =mat
#             else:
#                 image = np.concatenate((image,mat))
#
#         print(batch[1])
#         # hsv = cv2.cvtColor(mat,cv2.COLOR_RGB2HSV)
#         # mask = cv2.inRange(hsv, (9, 124, 100), (16, 250, 150))
#         cv2.imshow('test',image)
#
#
#         cv2.waitKey(0)

if __name__ == '__main__':
    generator = TrafficLiteGenerator(r'D:\Video_Log\traffic',r'D:\Video_Log\images')

    for i,image_name in enumerate(generator.image_paths):
        mat = cv2.imread(image_name)
        h, w, c = mat.shape
        annotation_name = image_name.replace('.png', '.xml')
        if os.path.isfile(annotation_name):
            id, xmin, ymin, xmax, ymax = generator.read_annotation_xml(annotation_name)
        elif os.path.isfile(annotation_name.replace('xml', 'json')):
            id, xmin, ymin, xmax, ymax = generator.read_annotation_json(annotation_name.replace('xml', 'json'))

        croped = mat[ymin:ymax, xmin:xmax]
        label = ['bg','left','right'][id]
        cv2.imwrite(r'D:\Video_Log\32x32\{}\{}.png'.format(label,i), croped)
        print('Write : {}'.format(label))
        mat = cv2.imread(random.choice(generator.image_false_paths))
        id = 0
        xmax = random.randint(32, w)
        xmin = max(xmax - 32, 0)
        ymax = random.randint(32, h)
        ymin = max(ymax - 32, 0)

        croped = mat[ymin:ymax, xmin:xmax]
        label = ['bg', 'left', 'right'][id]
        cv2.imwrite(r'D:\Video_Log\32x32\{}\{}.png'.format(label,i),croped)













