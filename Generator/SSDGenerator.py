import glob
import json
import os
import random
import numpy as np
import cv2
import xml.etree.ElementTree as ET


class SSDGenerator:
    def __init__(self, image_path, ssd_input_encoder):
        self.image_paths = glob.glob(r'{}/**/*rgb.png'.format(image_path))
        self.class_idx = {'left': 1, 'right': 2}
<<<<<<< HEAD
        self.ssd_input_encoder = ssd_input_encoder

=======
        self.ssd_input_encoder=  ssd_input_encoder
>>>>>>> b174537dedb364888e733c1cadd1bc9ec5209e05
    def read_annotation_xml(self, path):
        tree = ET.parse(path)
        root = tree.getroot()
        for p in root.findall('.//object'):
            id = self.class_idx[p.find('name').text]
            xmin = int(p.find('bndbox').find('xmin').text)
            ymin = int(p.find('bndbox').find('ymin').text)
            xmax = int(p.find('bndbox').find('xmax').text)
            ymax = int(p.find('bndbox').find('ymax').text)
            return id, xmin, ymin, xmax, ymax

    def read_annotation_json(self, path):
        json_data = json.loads(open(path).read())
        shapes = json_data['shapes']
        points = shapes[0]['points']
        x1, y1 = tuple(points[0])
        x2, y2 = tuple(points[1])
        xs = [x1, x2]
        ys = [y1, y2]
        xmin, ymin, xmax, ymax = min(xs), min(ys), max(xs), max(ys)

        id = self.class_idx[shapes[0]['label']]
        return id, xmin, ymin, xmax, ymax

    def generator(self, batch_size, mode='train', im_h=300, im_w=300):
        batch_images = np.zeros((batch_size, 300, 300, 3))
        while True:
            annotations = []
            for i in range(batch_size):
                image_name = random.choice(self.image_paths)

                mat = cv2.imread(image_name)
                h, w, c = mat.shape
                mat = cv2.resize(mat, (im_w, im_h))
                mat = mat / 255.
                # anno : label,xmin,ymin,xmax,ymax

                annotation_name = image_name.replace('.png', '.xml')
                if os.path.isfile(annotation_name):
                    id, xmin, ymin, xmax, ymax = self.read_annotation_xml(annotation_name)
                elif os.path.isfile(annotation_name.replace('xml', 'json')):
                    id, xmin, ymin, xmax, ymax = self.read_annotation_json(annotation_name.replace('xml', 'json'))

                break

                # id left 1, right 2
                if bool(random.getrandbits(1)):
                    id = 3 - id
                    xmax, xmin = w - xmin, w - xmax
                    mat = cv2.flip(mat, 0)

                annotations.append(np.array([[id, xmin, ymin, xmax, ymax]]))
                batch_images[i] = mat
            if mode is 'train':
                batch_annotions = self.ssd_input_encoder(annotations)
            else:
                batch_annotions = annotations
            yield batch_images, batch_annotions
