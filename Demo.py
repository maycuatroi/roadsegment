import glob
import random

import keras
from keras import applications
import cv2
import numpy as np
from keras.applications import MobileNetV2
from keras.callbacks import Callback
from keras.engine import Layer
from keras.layers import Dense, Flatten, Input, Concatenate, Permute, \
    AveragePooling2D, Conv2D, Dropout, MaxPooling2D, UpSampling2D, BatchNormalization, ELU, Reshape, Activation, \
    Deconvolution2D, ZeroPadding2D
from keras.models import Model
import tensorflow as tf
from keras.regularizers import l2
import keras.backend as K

from ssd_ultis.keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from ssd_ultis.keras_layers.keras_layer_DecodeDetections import DecodeDetections


def pun_net():
    road_input = Input((128, 128, 3))
    # semi_input = Conv2D(3,(3,3))
    base_model = MobileNetV2(
        include_top=False,
        weights = None,
        input_tensor=road_input,
        input_shape=(128,128, 3),
        pooling='avg')


    for layer in base_model.layers:
        layer.trainable = True  # trainable has to be false in order to freeze the layers

    # up1 = Concatenate()([UpSampling2D(size=(2, 2))(base_model.get_layer('out_relu').output),base_model.get_layer('block_13_expand_relu').output ])
    # x = Conv2D(64,(3,3), padding='same')(up1)
    # x = Dropout(0.2)(x)
    x = base_model.get_layer('block_13_expand_relu').output
    up2 = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('block_6_expand_relu').output])
    x = Conv2D(64,(3,3), padding='same')(up2)
    x = Dropout(0.2)(x)

    x = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('block_3_expand_relu').output])
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = Dropout(0.2)(x)

    x = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('block_1_expand_relu').output])
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = Dropout(0.2)(x)

    x = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('input_1').output])
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = Dropout(0.2)(x)

    x = UpSampling2D(size=(2, 2))(x)
    x = Conv2D(64,(3,3), padding='same')(x)
    x = Conv2D(2,(1,1),activation='sigmoid')(x)
    x = Permute((3,1,2))(x)
    # x  = base_model.output
    model = Model(road_input,x)
    model.compile(loss="binary_crossentropy", optimizer='adam', metrics=['accuracy'])

    return model


# def pun_net_reconstruction():


model = pun_net()
model.summary()
model.load_weights('weights/punnet_fastest.h5',by_name=True)

video_path = r"C:\Users\Binh Bum\Downloads\src\1554559047.97_rgb.avi" #path to test video
cap = cv2.VideoCapture(video_path)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('road2.avi',fourcc, 24.0, (512,256))
while True:
    # image = next(generator(1))[0][0]
    ret, mat = cap.read()
    if not ret:
        cap = cv2.VideoCapture(video_path)
        break
        # break
    road_image = cv2.resize(mat, (128, 128)) / 255.
    y = model.predict(np.array([road_image]))
    road = y[0][0]
    line = y[0][1]
    image = cv2.resize(mat, (256, 256))
    segment = np.zeros((256, 256, 3))
    segment[road > 0.5] = (0, 255, 0)
    segment[line > 0.5] = (0, 255, 255)
    segment = segment.astype(np.uint8)
    image = (image * 255.).astype(np.uint8)
    mat = cv2.resize(mat, (256, 256))
    out_image = np.concatenate((mat, segment), 1)
    out.write(out_image)
    cv2.imshow('test', out_image)
    k = cv2.waitKey(1)
    # if k ==ord('s'):
    #     writeout = True
    if k == 32:
        cv2.waitKey(0)
