#!/usr/bin/env python
import argparse
import base64
import glob

import cv2
import json
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

from labelme import utils
from tqdm import tqdm

PY2 = sys.version_info[0] == 2


def convert_json(json_file):
    data = json.load(open(json_file))
    if data['imageData']:
        imageData = data['imageData']
    else:
        imagePath = os.path.join(os.path.dirname(json_file), data['imagePath'])
        with open(imagePath, 'rb') as f:
            imageData = f.read()
            imageData = base64.b64encode(imageData).decode('utf-8')
    img = utils.img_b64_to_arr(imageData)
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    h,w,c = img.shape
    label_name_to_value = {'_background_': 0,'lane':1,'line':1,'road':2}
    # for shape in sorted(data['shapes'], key=lambda x: x['label']):
    #     label_name = shape['label']
    #     if label_name in label_name_to_value:
    #         label_value = label_name_to_value[label_name]
    #     else:
    #         label_value = len(label_name_to_value)
    #         label_name_to_value[label_name] = label_value
    newlist = sorted(data['shapes'], key=lambda k: k['label'],reverse=True)
    lbl = utils.shapes_to_label(img.shape, newlist, label_name_to_value)
    # lbl = np.zeros((h,w,3))

    # label_names = [None] * (max(label_name_to_value.values()) + 1)
    # for name, value in label_name_to_value.items():
    #     label_names[value] = name
    # lbl_viz = utils.draw_label(lbl, img, label_names)

    # mat = np.zeros((h,w))
    # mat[lbl==2]=(0,127,0)
    # mat[lbl==1]=(255,0,127)
    # cv2.imshow('label',mat)
    # cv2.imshow('image',lbl_viz)
    # cv2.waitKey(0)
    cv2.imwrite(r'F:\Video_Log\only_road\labels/'+json_file.split(os.sep)[-1].replace('json','png'),lbl)
    # plt.subplot(121)
    # plt.imshow(img)
    # plt.subplot(122)
    # plt.imshow(lbl_viz)
    # plt.show()


if __name__ == '__main__':
    json_paths=  glob.glob(r'F:\Video_Log\only_road\images\*.json')
    for json_file in tqdm(json_paths):
        # json_file = r"F:\Video_Log\images\2019-01-26_18-17-36-615796.json"
        convert_json(json_file)
