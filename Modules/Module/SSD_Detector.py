"""
Detector module use to run detect object on image
"""

import glob

import numpy as np
import tensorflow as tf
from cv2 import cv2
from object_detection.utils import label_map_util

from Modules.Entities.ImageDetected import ImageDetected
from Modules.Module import SSD_Detector as Dec
from Modules.Module.Detector import Detector


class SSD_Detector(Detector):

    detector = None
    def __init__(self, path_to_model):
        """"
          Init model and labels map
                 1 - graph model : Trained model
                 2 - label maps  : use to mapping label ID and label Name
        :param path_to_model: path to model directory
        """

        PATH_TO_CKPT = path_to_model + '/frozen_inference_graph.pb'

        PATH_TO_LABELS = glob.glob("{}/*.pbtxt".format(path_to_model))[0]
        # input(PATH_TO_LABELS)
        # need to change

        lines = open(PATH_TO_LABELS).read().split('\n')
        max_id = 0
        for line in lines:
            if 'id:' in line:
                id = int(line.split(':')[1].strip())
                if id > max_id:
                    max_id = id
        super().__init__()

        NUM_CLASSES = max_id
        print("NUM CLASSES : {}".format(NUM_CLASSES))
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        self.categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                                         use_display_name=True)
        self.category_index = label_map_util.create_category_index(self.categories)
        with self.detection_graph.as_default():
            self.sess = tf.Session(graph=self.detection_graph)
        Dec.detector = self

    def detect_object(self, image_np, frame=None):
        """
        Detect object in image
        :param image_np: numpy array mat
        :param frame: frame index
        :return: ImageDetected object contain mat,list of boundingbox, list of scores, frame index
        """
        image_np = cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR)
        image_np_expanded = np.expand_dims(image_np, axis=0)

        image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')

        boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
        (boxes, scores, classes, num_detections) = self.sess.run(
            [boxes, scores, classes, num_detections],
            feed_dict={image_tensor: image_np_expanded})

        image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
        return ImageDetected(image_np, boxes[0], classes[0], scores[0], frame=frame)
