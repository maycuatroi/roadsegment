import cv2
import numpy as np
import pandas as pd
from collections import namedtuple


def split(df, group):
    data = namedtuple('data', ['filename', 'object'])
    gb = df.groupby(group)
    return [data(filename, gb.get_group(x)) for filename, x in zip(gb.groups.keys(), gb.groups)]


def flip(mat):
    return cv2.flip(mat, 1)


annotations = {'filename': [], 'width': [], 'height': [], 'class': [], 'xmin': [], 'ymin': [], 'xmax': [], 'ymax': []}

examples = pd.read_csv('../data/annotation/train.csv')
grouped = split(examples, 'filename')
for group in grouped:
    for index, row in group.object.iterrows():
        if 'sign' not in group.filename:
            continue
        mat = cv2.imread('../data/jpg/{}'.format(group.filename))
        h, w, c = mat.shape
        fliped = cv2.flip(mat, 1)
        annotations['filename'].append(group.filename)
        annotations['xmin'].append(row['xmin'])
        annotations['xmax'].append(row['xmax'])
        annotations['ymin'].append(row['ymin'])
        annotations['ymax'].append(row['ymax'])
        annotations['class'].append(row['class'])
        annotations['width'].append(w)
        annotations['height'].append(h)

        annotations['filename'].append('fliped_{}'.format(group.filename))
        annotations['xmin'].append(w - row['xmax'])
        annotations['xmax'].append(w - row['xmin'])
        annotations['ymin'].append(row['ymin'])
        annotations['ymax'].append(row['ymax'])
        annotations['width'].append(w)
        annotations['height'].append(h)
        if 'phai' in row['class']:
            annotations['class'].append('retrai')
        elif 'trai' in row['class']:
            annotations['class'].append('rephai')
        else:
            annotations['class'].append(row['class'])
        cv2.rectangle(mat,(int(row['xmin']),int(row['ymin'])),(int(row['xmax']),int(row['ymax'])),(0,255,255))
        cv2.rectangle(fliped,(int(w - row['xmax']),int(row['ymin'])),(int(w - row['xmin']),int(row['ymax'])),(0,255,255))
        # cv2.imshow('test', np.concatenate((mat,fliped),1))
        cv2.imshow('test', mat)
        k = cv2.waitKey(0)
        if k == ord('s'):
            cv2.imwrite('/home/binhbumpro/{}'.format(group.filename), mat)
        # cv2.imwrite('../data/jpg/{}'.format(group.filename),mat)
        # cv2.imwrite('../data/jpg/fliped_{}'.format(group.filename).format(group.filename),fliped)
# t = pd.DataFrame(annotations.items())
# pd.DataFrame.from_dict(annotations).to_csv('../data/annotation/train.csv', sep=',', index=False,columns=['filename','width','height','class','xmin','ymin','xmax','ymax'])

