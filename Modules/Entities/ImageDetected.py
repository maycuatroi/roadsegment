import numpy as np
from object_detection.utils import visualization_utils as vis_util

from Modules.Entities.DetectedObject import DetectedObject
from Modules.Module.Detector import Detector


class ImageDetected:
    def __init__(self, image, boxes, classes, scores,frame = None):
        self.image = image
        self.boxes = boxes
        self.classes = classes
        self.scores = scores
        self.detected_objects = []
        self.frame = frame
        im_width, im_height = self.image.shape[0], self.image.shape[1]
        for i in range(len(self.boxes)):
            box = tuple(self.boxes[i].tolist())
            ymin, xmin, ymax, xmax = box
            if ymin * xmin * ymax *xmax <=1:
                xmin *= im_height
                xmax *= im_height
                ymin *= im_width
                ymax *= im_width
                box = (ymin, xmin, ymax, xmax)
            label_string = Detector.detector.category_index[self.classes[i]]
            self.detected_objects.append(DetectedObject(label_string, float(self.scores[i]), box,frame=self.frame))

    def visualize(self):
        vis_util.visualize_boxes_and_labels_on_image_array(
            self.image,
            np.squeeze(self.boxes),
            np.squeeze(self.classes).astype(np.int32),
            np.squeeze(self.scores),
            Detector.detector.category_index,
            use_normalized_coordinates=True,
            line_thickness=2, min_score_thresh=0.)
        return self.image

    def crop_image(self):
        for objectdetected in self.detected_objects:
            ymin, xmin, ymax, xmax = objectdetected.box
            objectdetected.image = self.image[int(ymin):int(ymax), int(xmin):int(xmax)]
