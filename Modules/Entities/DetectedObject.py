import json
import math

from cv2 import cv2


class DetectedObject:
    def __init__(self, label, score, box, image=None, frame=None):
        self.label = label
        self.score = score
        self.box = box
        self.image = image
        self.frame = frame

    # Kiểm tra tỷ lệ 2 cạnh
    def BoxRatio(self):
        """
        :return: check Box ratio
        """
        ymin, xmin, ymax, xmax = self.box
        width = ymax - ymin
        height = xmax - xmin
        ratio = width / height
        if ratio < 1:
            ratio = 1 / ratio
        return ratio

    def GetCentroid(self):
        """
        :return: Return center of object bounding box
        """
        ymin, xmin, ymax, xmax = self.box
        return (int((xmax + xmin) / 2), int((ymax + ymin) / 2))

    def isInside(self, point):
        if point is None:
            return False
        x, y = point
        ymin, xmin, ymax, xmax = self.box
        if x > xmin and x < xmax and y > ymin and y < ymax:
            return True
        return False

    def getdistance(self, _object):
        oCentroid = _object.GetCentroy()
        return self.getdistance(point=oCentroid)

    def getdistance(self, point):
        centroid = self.GetCentroid()
        x1, y1 = centroid
        x2, y2 = point

        return math.hypot(x2 - x1, y2 - y1)

    def get_box_area(self):
        ymin, xmin, ymax, xmax = self.box
        return (xmax - xmin) * (ymax - ymin)

    def getClosestObject(self, objects):
        closest_distance = None
        closest_object = None
        for _object in objects:
            oCentroid = _object.GetCentroy()
            if self.isInside(oCentroid):
                distance = self.getdistance(point=oCentroid)
                if closest_distance is None or closest_distance < distance:
                    closest_distance = distance
                    closest_object = _object
        return closest_object

    def visualize(self, image, color=(0, 255, 0), thickness=2):
        ymin, xmin, ymax, xmax = self.box

        # self.image = image[int(ymin):int(ymax), int(xmin):int(xmax)]

        cv2.rectangle(image, (int(xmax), int(ymax)), (int(xmin), int(ymin)), color, thickness)
        font = cv2.FONT_ITALIC

        label_text = self.label['name'] + '   ' + str(round(self.score * 100))

        cv2.putText(image, label_text, (int(xmin), int(ymin - 20)), font, 1, (255, 255, 255), 2,
                    cv2.LINE_AA)
        return image

    def circle_visualize(self, image, color=(0, 255, 0), thickness=2):
        ymin, xmin, ymax, xmax = self.box
        center = self.GetCentroid()
        radius = max(xmax - center[0], ymax - center[1])

        cv2.circle(image, center, int(radius), color, thickness)
        font = cv2.FONT_ITALIC
        label_text = self.label['name'] + '   ' + str(round(self.score * 100))

        cv2.putText(image, label_text, (int(xmin), int(ymin - 20)), font, 1, (255, 255, 255), 2,
                    cv2.LINE_AA)
        return image

    def toJson(self):
        data = {}
        data['label'] = self.label
        data['score'] = self.score
        data['bbox'] = self.box
        return json.dumps(data)

    def to_dict(self):
        data = {}
        data['label'] = self.label
        data['score'] = self.score
        data['bbox'] = self.box
        return data

    def __repr__(self) -> str:

        return self.toJson()
