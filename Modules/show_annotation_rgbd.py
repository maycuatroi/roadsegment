import cv2
import glob
import numpy as np
import pandas as pd
from collections import namedtuple


def split(df, group):
    data = namedtuple('data', ['filename', 'object'])
    gb = df.groupby(group)
    return [data(filename, gb.get_group(x)) for filename, x in zip(gb.groups.keys(), gb.groups)]


image_patch = glob.glob(r'F:\Video_Log\left/*_rgb.png')

examples = pd.read_csv("F:\Video_Log\left\left.csv")
grouped = split(examples, 'filename')

for i in range(0,len(grouped),2):
    rgb_group = grouped[i]
    depth_group = grouped[i+1]
    for index, row in rgb_group.object.iterrows():
        rgb = cv2.imread('F:\Video_Log\left/{}'.format(rgb_group.filename))
        cv2.rectangle(rgb,(int(row['xmin']),int(row['ymin'])),(int(row['xmax']),int(row['ymax'])),(0,255,255))
        rgb_center = row['xmax']-row['xmin'],row['ymax']-row['ymin']
    for index, row in depth_group.object.iterrows():
        depth = cv2.imread('F:\Video_Log\left/{}'.format(depth_group.filename))

        cv2.rectangle(depth,(int(row['xmin']),int(row['ymin'])),(int(row['xmax']),int(row['ymax'])),(0,255,255))
        depth_center = row['xmax']-row['xmin'],row['ymax']-row['ymin']

    cv2.imshow('rgb', rgb)
    cv2.imshow('test', depth)
    k = cv2.waitKey(0)


for path in image_patch:
    rgb = cv2.imread(path)
    depth = cv2.imread(path.replace('rgb','depth'),0)

    rgb[depth==0]=(255,255,255)




    cv2.imshow('rgb',rgb)
    cv2.imshow('depth',depth)
    cv2.waitKey(1)