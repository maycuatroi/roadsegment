import glob
import json
import os

import numpy as np
import cv2
from keras.models import load_model
from tqdm import tqdm


class Segment2Label:
    def __init__(self,output_json_path):
        self.dumy_data = {}
        self.json_path = output_json_path
        self.__init__data()
    def __init__data(self):
        data = {}
        data['version'] = '3.6.16'
        data['flags'] = {}
        data['shapes'] = []
        data['lineColor'] = [0, 255, 0, 128]
        data['fillColor'] = [255, 0, 0, 128]
        data['imagePath'] = None
        data['imageData'] = None
        data['imageHeight'] = 480
        data['imageWidth'] = 640
        self.dumy_data = data


    def mat2label(self, mat, label):
        mat[mat>=0.3] = 1
        mat[mat<0.3] = 0
        mat = mat.astype(np.uint8)

        im2, contours, hierarchy = cv2.findContours(mat, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cntsSorted = sorted(contours, key=lambda x: cv2.contourArea(x),reverse=True)
        cntsSorted = cntsSorted[:2]
        shapes = []

        for cnt in cntsSorted:
            epsilon = 0.005*cv2.arcLength(cnt,True)
            cnt = cv2.approxPolyDP(cnt,epsilon,True)
            shape ={}
            shape['label'] = label
            shape['line_color'] = None
            shape['fill_color'] = None
            shape['shape_type'] = 'polygon'
            shape['points'] = []
            xs,ys = tuple(cnt.reshape((-1,2)).T)
            for i in range(len(xs)):
                shape['points'].append([int(xs[i]),int(ys[i])])
            # shape['points'] = cnt
            shapes.append(shape)
        return shapes

    def segment2label(self, road, line,image_name):
        shapes = []
        road = cv2.resize(road,(640,480))
        line = cv2.resize(line,(640,480))

        shapes.extend(self.mat2label(road,'road'))
        shapes.extend(self.mat2label(line,'line'))
        self.dumy_data['imagePath'] = image_name
        self.dumy_data['shapes'] = shapes

        out_json_path = self.json_path+'/{}.json'.format(image_name.split('.')[0])
        json.dump(self.dumy_data, open(out_json_path,'w'))
        return self.dumy_data



if __name__ == '__main__':
    sq2lb = Segment2Label(r'F:\Video_Log\new_image')
    model = load_model('../model_binary/punnet_tiny.hdf5')
    image_paths = glob.glob(r'F:\Video_Log\new_image\*.png')
    batch_size = 16
    input_shape = model.layers[0].input_shape[1]

    bath = np.zeros((batch_size,input_shape,input_shape,3))

    for j in tqdm(range(0,len(image_paths),batch_size)):
        for i in range(batch_size):
            if j+i >= len(image_paths):
                break
            mat = cv2.imread(image_paths[j+i])
            image_input = cv2.resize(mat,(input_shape,input_shape))
            bath[i] = image_input
        bath= bath/255.
        y = model.predict(bath)


        for i in range(batch_size):
            if j+i >= len(image_paths):
                break
            road,line = y[i][0],y[i][1]
            sq2lb.segment2label(road,line,image_paths[j+i].split(os.sep)[-1])