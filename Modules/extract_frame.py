from datetime import time

import cv2
import numpy as np
import glob
from tqdm import tqdm

video_paths = glob.glob(r'C:\Users\Binh Bum\Downloads\src/*rgb.avi')
i = 0
for video_path in video_paths:
    cap = cv2.VideoCapture(video_path)
    step = 5

    pre = '07_04_4'
    lengh = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    for j in tqdm(range(lengh)):
        ret, mat = cap.read()
        if not ret:
            break

        i+=1
        if i % step == 0:
            # cv2.imshow('tét',mat)
            # cv2.waitKey(1)
            cv2.imwrite(r'F:\Video_Log\new_image\{}_{}.png'.format(pre, i), mat)

print('Finish')
