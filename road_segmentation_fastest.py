import glob
import random

import keras
from keras import applications
import cv2
import numpy as np
from keras.applications import MobileNetV2
from keras.callbacks import Callback
from keras.engine import Layer
from keras.layers import Dense, Flatten, Input, Concatenate, Permute, \
    AveragePooling2D, Conv2D, Dropout, MaxPooling2D, UpSampling2D, BatchNormalization, ELU, Reshape, Activation, \
    Deconvolution2D, ZeroPadding2D
from keras.models import Model
import tensorflow as tf
from keras.regularizers import l2
import keras.backend as K

from ssd_ultis.keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from ssd_ultis.keras_layers.keras_layer_DecodeDetections import DecodeDetections



data_source = r'F:\Video_Log\only_road/'
label_paths = glob.glob(data_source + 'labels/*.png')


def load_segment(label_name):
    segment = cv2.imread(label_name, 0)
    return segment



def generator_no_augment(batch_size):
    batch_features = np.zeros((batch_size, 128, 128, 3))
    batch_segment = np.zeros((batch_size, 2, 256, 256))
    while True:
        for i in range(batch_size):
            label_name = random.choice(label_paths)
            image_name = label_name.replace('labels', 'images')
            if '-' in image_name:
                image_name = image_name.replace('png', 'jpg')
            mat = cv2.imread(image_name)
            mat = cv2.resize(mat, (128, 128))
            segment = load_segment(label_name)
            segment = cv2.resize(segment, (256, 256))
            road = np.zeros((256, 256))
            line = np.zeros((256, 256))
            road[segment == 2] = 1
            line[segment == 1] = 1
            batch_features[i] = mat / 255.
            batch_segment[i] = np.array([road, line])
        yield batch_features, batch_segment


class My_Callback(Callback):

    def on_epoch_end(self, epoch, logs={}):
        bath = next(generator_no_augment(1))
        image = bath[0][0]

        y = self.model.predict(np.array([image]))
        image *= 255.
        # y *= 255.
        road = y[0][0]
        line = y[0][1]

        image = cv2.resize(image, (256, 256))
        segment = np.zeros((256, 256, 3))
        segment[road > 0.5] = (0, 255, 0)
        segment[line > 0.5] = (0, 255, 255)
        cv2.imwrite('log/{}.jpg'.format(epoch), np.concatenate((image, segment), 1))
        self.model.save_weights('weights/punnet_fastest.h5')
        return


def fucked_fake_model():
    image_input = Input((100, 100, 3))
    # crop = Cropping2D(cropping=((0, 84), (0, 0)), data_format='channels_last')(image_input)

    x = Conv2D(32, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv1')(image_input)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv2')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(64, (3, 3), strides=1, activation='relu', padding='same', name='seg_conv2')(x)
    x = AveragePooling2D(pool_size=(2, 2))(x)
    x = Flatten()(x)
    x = Dense(128, activation='relu')(x)
    x = Dense(1, activation='sigmoid')(x)
    model = Model(input=image_input, output=x)
    model.compile(loss="binary_crossentropy", optimizer='adam', metrics=['accuracy'])
    return model


def build_ssd_model(image_size=(300, 300, 3),
              n_classes=2,
              mode='training',
              l2_regularization=0.0,
              scales=[0.08, 0.16, 0.32, 0.64, 0.96],
              two_boxes_for_ar1=True,
              clip_boxes=False,
              variances=[1.0, 1.0, 1.0, 1.0],
              coords='centroids',
              normalize_coords=False,
              confidence_thresh=0.01,
              iou_threshold=0.45,
              top_k=200,
              nms_max_output_size=400,
              return_predictor_sizes=False):
    number_predictor = 4
    steps = [None] * number_predictor
    offsets = [None] * number_predictor
    n_classes += 1
    l2_reg = l2_regularization
    img_height, img_width, img_channels = image_size[0], image_size[1], image_size[2]

    x = Input(shape=(img_height, img_width, img_channels))
    conv1 = Conv2D(32, (5, 5), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv1')(x)
    conv1 = BatchNormalization(axis=3, momentum=0.99, name='bn1')(
        conv1)
    conv1 = ELU(name='elu1')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2), name='pool1')(conv1)

    conv2 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv2')(pool1)
    conv2 = BatchNormalization(axis=3, momentum=0.99, name='bn2')(conv2)
    conv2 = ELU(name='elu2')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2), name='pool2')(conv2)

    conv3 = Conv2D(64, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv3')(pool2)
    conv3 = BatchNormalization(axis=3, momentum=0.99, name='bn3')(conv3)
    conv3 = ELU(name='elu3')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2), name='pool3')(conv3)

    conv4 = Conv2D(64, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv4')(pool3)
    conv4 = BatchNormalization(axis=3, momentum=0.99, name='bn4')(conv4)
    conv4 = ELU(name='elu4')(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2), name='pool4')(conv4)

    conv5 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv5')(pool4)
    conv5 = BatchNormalization(axis=3, momentum=0.99, name='bn5')(conv5)
    conv5 = ELU(name='elu5')(conv5)
    pool5 = MaxPooling2D(pool_size=(2, 2), name='pool5')(conv5)

    conv6 = Conv2D(48, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv6')(pool5)
    conv6 = BatchNormalization(axis=3, momentum=0.99, name='bn6')(conv6)
    conv6 = ELU(name='elu6')(conv6)
    pool6 = MaxPooling2D(pool_size=(2, 2), name='pool6')(conv6)

    conv7 = Conv2D(32, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                   kernel_regularizer=l2(l2_reg), name='conv7')(pool6)
    conv7 = BatchNormalization(axis=3, momentum=0.99, name='bn7')(conv7)
    conv7 = ELU(name='elu7')(conv7)

    classes4 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes4')(conv4)
    classes5 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes5')(conv5)
    classes6 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes6')(conv6)
    classes7 = Conv2D(4 * n_classes, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                      kernel_regularizer=l2(l2_reg), name='classes7')(conv7)
    # Output shape of `boxes`: `(batch, height, width, n_boxes * 4)`
    boxes4 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes4')(conv4)
    boxes5 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes5')(conv5)
    boxes6 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes6')(conv6)
    boxes7 = Conv2D(4 * 4, (3, 3), strides=(1, 1), padding="same", kernel_initializer='he_normal',
                    kernel_regularizer=l2(l2_reg), name='boxes7')(conv7)

    anchors4 = AnchorBoxes(img_height, img_width, this_scale=scales[0], next_scale=scales[1],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[0], this_offsets=offsets[0],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors4')(boxes4)
    anchors5 = AnchorBoxes(img_height, img_width, this_scale=scales[1], next_scale=scales[2],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[1], this_offsets=offsets[1],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors5')(boxes5)
    anchors6 = AnchorBoxes(img_height, img_width, this_scale=scales[2], next_scale=scales[3],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[2], this_offsets=offsets[2],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors6')(boxes6)
    anchors7 = AnchorBoxes(img_height, img_width, this_scale=scales[3], next_scale=scales[4],
                           two_boxes_for_ar1=two_boxes_for_ar1, this_steps=steps[3], this_offsets=offsets[3],
                           clip_boxes=clip_boxes, variances=variances, coords=coords, normalize_coords=normalize_coords,
                           name='anchors7')(boxes7)

    classes4_reshaped = Reshape((-1, n_classes), name='classes4_reshape')(classes4)
    classes5_reshaped = Reshape((-1, n_classes), name='classes5_reshape')(classes5)
    classes6_reshaped = Reshape((-1, n_classes), name='classes6_reshape')(classes6)
    classes7_reshaped = Reshape((-1, n_classes), name='classes7_reshape')(classes7)
    boxes4_reshaped = Reshape((-1, 4), name='boxes4_reshape')(boxes4)
    boxes5_reshaped = Reshape((-1, 4), name='boxes5_reshape')(boxes5)
    boxes6_reshaped = Reshape((-1, 4), name='boxes6_reshape')(boxes6)
    boxes7_reshaped = Reshape((-1, 4), name='boxes7_reshape')(boxes7)
    anchors4_reshaped = Reshape((-1, 8), name='anchors4_reshape')(anchors4)
    anchors5_reshaped = Reshape((-1, 8), name='anchors5_reshape')(anchors5)
    anchors6_reshaped = Reshape((-1, 8), name='anchors6_reshape')(anchors6)
    anchors7_reshaped = Reshape((-1, 8), name='anchors7_reshape')(anchors7)
    classes_concat = Concatenate(axis=1, name='classes_concat')([classes4_reshaped,
                                                                 classes5_reshaped,
                                                                 classes6_reshaped,
                                                                 classes7_reshaped])
    boxes_concat = Concatenate(axis=1, name='boxes_concat')([boxes4_reshaped,
                                                             boxes5_reshaped,
                                                             boxes6_reshaped,
                                                             boxes7_reshaped])
    anchors_concat = Concatenate(axis=1, name='anchors_concat')([anchors4_reshaped,
                                                                 anchors5_reshaped,
                                                                 anchors6_reshaped,
                                                                 anchors7_reshaped])
    classes_softmax = Activation('softmax', name='classes_softmax')(classes_concat)
    predictions = Concatenate(axis=2, name='predictions')([classes_softmax, boxes_concat, anchors_concat])

    if mode == 'training':
        model = Model(inputs=x, outputs=predictions)
    elif mode == 'inference':
        decoded_predictions = DecodeDetections(confidence_thresh=confidence_thresh,
                                               iou_threshold=iou_threshold,
                                               top_k=top_k,
                                               nms_max_output_size=nms_max_output_size,
                                               coords=coords,
                                               normalize_coords=normalize_coords,
                                               img_height=img_height,
                                               img_width=img_width,
                                               name='decoded_predictions')(predictions)
        model = Model(inputs=x, outputs=decoded_predictions)

    if return_predictor_sizes:
        # The spatial dimensions are the same for the `classes` and `boxes` predictor layers.
        predictor_sizes = np.array([classes4._keras_shape[1:3],
                                    classes5._keras_shape[1:3],
                                    classes6._keras_shape[1:3],
                                    classes7._keras_shape[1:3]])
        return model, predictor_sizes
    else:
        return model


def image_func(img):
    img = cv2.resize(img, (100, 100))
    return img.astype('float32')


def image_tensor_func(img4d):
    results = []
    for img3d in img4d:
        rimg3d = image_func(img3d)
        results.append(np.expand_dims(rimg3d, axis=0))
    return np.concatenate(results, axis=0)


class Custom_resize_Layer(Layer):
    def call(self, xin):
        xout = tf.py_func(image_tensor_func,
                          [xin],
                          'float32',
                          stateful=False,
                          name='cvOpt')
        xout = K.stop_gradient(xout)  # explicitly set no grad
        xout.set_shape([xin.shape[0], 100, 100, xin.shape[-1]])  # explicitly set output shape
        return xout

    def compute_output_shape(self, sin):
        return (sin[0], 100, 100, sin[-1])


def pun_net():
    road_input = Input((128, 128, 3))
    # semi_input = Conv2D(3,(3,3))
    base_model = MobileNetV2(
        include_top=False,
        weights = None,
        input_tensor=road_input,
        input_shape=(128,128, 3),
        pooling='avg')


    for layer in base_model.layers:
        layer.trainable = True  # trainable has to be false in order to freeze the layers

    # up1 = Concatenate()([UpSampling2D(size=(2, 2))(base_model.get_layer('out_relu').output),base_model.get_layer('block_13_expand_relu').output ])
    # x = Conv2D(64,(3,3), padding='same')(up1)
    # x = Dropout(0.2)(x)
    x = base_model.get_layer('block_13_expand_relu').output
    up2 = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('block_6_expand_relu').output])
    x = Conv2D(64,(3,3), padding='same')(up2)
    x = Dropout(0.2)(x)

    x = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('block_3_expand_relu').output])
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = Dropout(0.2)(x)

    x = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('block_1_expand_relu').output])
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = Dropout(0.2)(x)

    x = Concatenate()([UpSampling2D(size=(2, 2))(x), base_model.get_layer('input_1').output])
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = Dropout(0.2)(x)

    x = UpSampling2D(size=(2, 2))(x)
    x = Conv2D(64,(3,3), padding='same')(x)
    x = Conv2D(2,(1,1),activation='sigmoid')(x)
    x = Permute((3,1,2))(x)
    # x  = base_model.output
    model = Model(road_input,x)
    model.compile(loss="binary_crossentropy", optimizer='adam', metrics=['accuracy'])

    return model


# def pun_net_reconstruction():


model = pun_net()
model.summary()
model.load_weights('weights/punnet_fastest.h5',by_name=True)
callback = My_Callback()


# model.fit_generator(generator_no_augment(16), steps_per_epoch=200, epochs=15, callbacks=[callback])
# model.save('model_binary/mobilenet.h5')

video_path = r"C:\Users\Binh Bum\Downloads\src\1554467152.87_rgb.avi" #path to test video
cap = cv2.VideoCapture(video_path)

while True:
    # image = next(generator(1))[0][0]
    ret, mat = cap.read()
    if not ret:
        cap = cv2.VideoCapture(video_path)
        continue
        # break
    road_image = cv2.resize(mat, (128, 128)) / 255.
    y = model.predict(np.array([road_image]))
    # y_seg = y[0]
    # y_sign = y[1]

    # y= y_seg
    road = y[0][0]
    line = y[0][1]
    image = cv2.resize(mat, (256, 256))
    segment = np.zeros((256, 256, 3))
    segment[road > 0.5] = (0, 255, 0)
    segment[line > 0.5] = (0, 255, 255)
    segment = segment.astype(np.uint8)
    # image = np.concatenate((image,road),1)
    image = (image * 255.).astype(np.uint8)
    mat = cv2.resize(mat, (256, 256))
    out_image = np.concatenate((mat, segment), 1)
    cv2.imshow('test', out_image)
    k = cv2.waitKey(1)
    # if k ==ord('s'):
    #     writeout = True
    if k == 32:
        cv2.waitKey(0)
