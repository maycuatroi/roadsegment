import numpy as np
import cv2 as cv
import math
import time
import Config
from Entities.Obstacle import Obstacle


class Xuly:
    def __init__(self):
        self.skyline = Config.skyline
        self.kernel_depth = Config.kernel_depth
        self.layer_h = Config.layer_h
        self.do_rong_duong = Config.do_rong_duong
        self.lower_chau_cay = Config.lower_chau_cay
        self.upper_chau_cay = Config.upper_chau_cay
        self.lower_lane = Config.lower_lane
        self.upper_lane = Config.upper_lane
        self.src_w = Config.src_w
        self.src_h = Config.src_h

        self.birdview_w = Config.birdview_w
        self.birdview_h = Config.birdview_h
        self.margin_bottom = Config.margin_bottom

        self.src_vertices = np.array([[0, 0], [self.src_w, 0], [self.src_w, self.src_h], [0, self.src_h]], np.float32)
        self.dst_vertices = np.array(
            [[0, 0], [self.birdview_w, 0], [self.birdview_w - self.margin_bottom, self.birdview_h],
             [self.margin_bottom, self.birdview_h]], np.float32)

        self.M = cv.getPerspectiveTransform(self.src_vertices, self.dst_vertices)
        self.kernel_duong = np.ones([9, 9], np.uint8)

        self.mode_chau_cay = True

        # trong ham findCenterpoints
        self.h = self.birdview_h - 80
        # area, t0, t1, b0, b1, l0, l1, r0, r1
        self.cnt_trai_layer_truoc = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])
        self.cnt_phai_layer_truoc = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])
        self.split_layers_range = range(self.h, 0, -self.layer_h)
        self.center_point = np.array([self.birdview_w // 2 - 1, 0])
        self.left_lane = np.zeros((len(range(self.h, 0, -self.layer_h)), 2))
        self.right_lane = np.zeros((len(range(self.h, 0, -self.layer_h)), 2))
        self.center_lane = np.zeros((len(range(self.h, 0, -self.layer_h)), 2))
        self.res_image = None
        self.res_image_deph = None
        self.w = None
        self.half_width = None
        self.thongtin_vatcan = np.array([0, -1, -1, -1, -1])

        self.obstacle = Obstacle()

    def process_vat_can(self, depth_frame, road_img):
        if Config.ne_vat_can:
            inrange_vatcan_img = self.timVatCanTrongAnhDepth(depth_frame)
            vatcan_img_bv, thongtin_vatcan = self.preProcessVatCan(inrange_vatcan_img)
            if self.obstacle.exist:
                road_img = cv.bitwise_or(road_img, vatcan_img_bv)  # ghep anh duong va anh vat can
            vat_can_thuoc_lane = self.obstacle.vat_can_thuoc_lane_nao(self.left_lane, self.right_lane)
            return road_img, vat_can_thuoc_lane
        return road_img, None

    def run(self, bgr_frame, depth_frame, debug, traffic_sign, old_speed):
        crop = bgr_frame[self.skyline:, :]
        gray_frame = cv.cvtColor(crop, cv.COLOR_BGR2GRAY)
        hsv_frame = cv.cvtColor(crop, cv.COLOR_BGR2HSV)
        _, thresh = cv.threshold(gray_frame, 240, 255, cv.THRESH_BINARY)
        denoise_image = self.deNoise(thresh)
        if self.mode_chau_cay:
            inrange_chau_cay = cv.inRange(hsv_frame, self.lower_chau_cay, self.upper_chau_cay)
            # inrange_chau_cay = cv.inRange(gray_frame, 30, 80)
            denoise_image = cv.bitwise_or(denoise_image, inrange_chau_cay)

        road_img = self.birdviewTransform(denoise_image)[80:, :]

        road_img, vat_can_thuoc_lane = self.process_vat_can(depth_frame, road_img)

        if self.mode_chau_cay:
            # inrange_chau_cay = cv.morphologyEx(inrange_chau_cay, cv.MORPH_OPEN, kernel_chau_cay)
            co_chau_cay = (np.sum(inrange_chau_cay != 0) > 2000)
            if co_chau_cay:
                print('co chau cay ', co_chau_cay)
        else:
            co_chau_cay = False

        if traffic_sign != 0:
            road_img = self.catDuongNgaBa(road_img, traffic_sign, debug)

        center_points = self.findCenterPoints(road_img, traffic_sign, debug, thongtin_vatcan=None)

        if False:
            reg = np.polyfit(400 - center_points[:4, 1], center_points[:4, 0], 1)
            angle = np.arctan(reg[0]) / np.pi * 180
            angle = angle * 2.5
        else:
            #### False: tinh goc theo cach oldschool (so sanh cac diem voi diem giua cua anh birdview)
            if self.w is None:
                self.w = road_img.shape[1]
                self.half_width = self.w // 2 - 1
            angle = self.calculateSteerAngle(center_points)

        if not co_chau_cay:
            speed = self.calculateSpeed(angle, old_speed)
        else:
            speed = 15.0

        return angle, speed, co_chau_cay

    def check_obstacle(self):
        distance2left = self.obstacle.distance2lane(self.left_lane)
        return

    def knn(self, lefts, rights, center):
        dleft = np.sum((lefts - center) ** 2, axis=1)
        dright = np.sum((rights - center) ** 2, axis=1)
        k = len(lefts)
        if k <= 0:
            return 0
        if k % 2 == 0:
            k -= 1
        topk = np.hstack((dleft, dright)).argsort() < k
        l, r = np.split(topk, 2)
        count_left = np.sum(l)
        count_right = np.sum(r)
        if count_left > count_right:
            return -1
        elif count_right > count_left:
            return 1
        else:
            #### GG
            return 0

    def preProcessVatCan(self, img):
        '''
        Xoa nhung contour nho trong anh vat can va nhung vat can qua xa so voi xe.
        Tra ve: anh birdview chua vat can gan nhat so voi xe, vitri cua vat can do.
        '''

        ### vitri_vatcan (0 hay 1), x bottom contour vatcan, y bottom contour vatcan, x tam vat can, y tam vat can
        self.thongtin_vatcan[:] = [0, -1, -1, -1, -1]
        vatcan_img = self.birdviewTransform(img[self.skyline:, :])[80:, :]
        vatcan_img[:100, :] = 0
        # half_w = self.birdview_w // 2 - 1
        index_bottom_max = -1
        bottom_max_y = 0
        _, contours, _ = cv.findContours(vatcan_img, cv.RETR_LIST, cv.CHAIN_APPROX_NONE)
        vatcan_img[:] = 0
        for i, cnt in enumerate(contours):
            cnt_area, t, b, l, r = self.timThongTinCuaMotContour(cnt)
            if cnt_area <= 50 or b[1] < 250:
                ## so co the thay doi, dung de loai bo nhung contour xa so voi xe
                continue
            if bottom_max_y <= b[1]:
                index_bottom_max = i
                bottom_max_y = b[1]
                self.thongtin_vatcan[0] = 1  # confirm co vat can trong duong

                ### kiem tra vitri vat can de gan gia tri diem cat
                if self.thongtin_vatcan[0] == -1:
                    ## cat ben trai, lay diem ben phai cua cnt
                    self.thongtin_vatcan[1] = r[0]
                    self.thongtin_vatcan[2] = r[1]
                else:
                    self.thongtin_vatcan[1] = l[0]
                    self.thongtin_vatcan[2] = l[1]
                self.thongtin_vatcan[4] = (t[1] + b[1]) // 2
                self.thongtin_vatcan[3] = (l[0] + r[0]) // 2

        if index_bottom_max != -1:
            cv.drawContours(vatcan_img, contours, index_bottom_max, 255, -1)
            x, y, w, h = cv.boundingRect(vatcan_img)
            self.obstacle.xmin = x
            self.obstacle.ymin = y
            self.obstacle.xmax = x + w
            self.obstacle.ymax = y + h
            self.obstacle.exist = True
        else:
            self.obstacle.exist = False

            # rgb = cv.cvtColor(vatcan_img,cv.COLOR_GRAY2RGB)
            # cv.rectangle(rgb,(x,y),(x+w,y+h),(0,255,255),2)
            # cv.imshow('object',rgb)

        return vatcan_img, self.thongtin_vatcan

    def catDuongNgaBa(self, road_image, traffic_sign, debug):
        giua_man_hinh = road_image.shape[1] // 2 - 1
        h = road_image.shape[0]

        try:
            #### toa do y cua diem mau trang duoi cung, tinh tu tren xuong.
            diem_cat_y = np.where(road_image[:, giua_man_hinh] != 0)[0].max()
        except Exception:
            diem_cat_y = 0
        if traffic_sign == -1:
            ## re trai, cat duong ben phai
            # cat het ben phai, cho nua man hinh ben phai thanh mau den
            road_image[:, giua_man_hinh:] = 0
            # ve them lane sau khi da bi cat
            cv.line(road_image, (giua_man_hinh, diem_cat_y), (giua_man_hinh + self.do_rong_duong, h - 1), 255, 5)
        elif traffic_sign == 1:
            ## re phai, cat duong ben trai
            road_image[:, :giua_man_hinh] = 0
            cv.line(road_image, (giua_man_hinh, diem_cat_y), (giua_man_hinh - self.do_rong_duong, h - 1), 255, 5)
        return road_image

    def timVatCanTrongAnhDepth(self, depth_frame):
        """
        Ham nhan vao anh depth tu topic
        output:
        """
        if self.res_image_deph is None:
            self.res_image_deph = np.zeros_like(depth_frame)
        depth_crop = depth_frame[200:400, 30:-60]
        # depth_crop = (np.floor(depth_crop / 30) * 30).astype(np.uint8)
        depth_crop = np.floor(depth_crop / 30) * 30
        depth_mean = np.array([depth_crop.mean(axis=1)]).T
        mean_low = depth_mean - 15
        depth_crop[depth_crop < mean_low] = 0
        depth_crop[depth_crop >= mean_low] = 255
        depth_crop = 255 - depth_crop
        depth_crop = cv.morphologyEx(depth_crop, cv.MORPH_OPEN, self.kernel_depth)  ## ham thay the chi erode + dilate
        self.res_image_deph[200:400, 30:-60] = depth_crop
        return self.res_image_deph

    def birdviewTransform(self, binary_image):
        return cv.warpPerspective(binary_image, self.M, (self.birdview_w, self.birdview_h), cv.INTER_LINEAR,
                                  cv.BORDER_CONSTANT)

    def deNoise(self, thresh_image):
        """
        Loai bo nhung contour thua truoc khi cho vao` birdview
        """
        _, contours, _ = cv.findContours(thresh_image, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        if self.res_image is None:
            self.res_image = np.zeros_like(thresh_image)
        else:
            self.res_image[:] = 0
        denoised_contours = []
        for cnt in contours:
            if cv.contourArea(cnt) > 100 and cv.arcLength(cnt, False) > 500:
                denoised_contours.append(cnt)

        cv.drawContours(self.res_image, denoised_contours, -1, 255, -1)
        return self.res_image

    def findCenterPoints(self, birdview_image, traffic_sign, debug, thongtin_vatcan ):
        """
        vatcan_img la anh birdview cua vat can, co kich thuoc = birdview_image
        """

        self.cnt_phai_layer_truoc[:] = 0
        self.cnt_trai_layer_truoc[:] = 0
        self.center_point[:] = [self.birdview_w // 2 - 1, 0]

        for i, y in enumerate(self.split_layers_range):
            self.timLanDuongTraiPhaiTai1Layer(
                birdview_image[y - self.layer_h:y, :], self.h, y,
                self.center_point)
            self.left_lane[i] = [self.cnt_trai_layer_truoc[7], self.cnt_trai_layer_truoc[8]]
            self.right_lane[i] = [self.cnt_phai_layer_truoc[5], self.cnt_phai_layer_truoc[6]]
            self.center_lane[i] = self.center_point

            if self.cnt_trai_layer_truoc[7] >= self.cnt_phai_layer_truoc[5] or self.cnt_phai_layer_truoc[5] <= \
                    self.cnt_trai_layer_truoc[
                        7] or abs(self.cnt_trai_layer_truoc[7] - self.cnt_phai_layer_truoc[5]) < 20:
                self.center_lane[i + 1:] = -1
                self.right_lane[i + 1:] = -1
                self.center_lane[i + 1:] = -1
                # cv.waitKey(0)
                break

        if traffic_sign != 0:
            if traffic_sign == -1:
                ## bam lane trai
                self.center_lane[:, 0] = self.left_lane[:, 0] + 25
            elif traffic_sign == 1:
                ## bam lane phai
                self.center_lane[:, 0] = self.right_lane[:, 0] - 25

        #### logic ne vat can ###
        elif False and thongtin_vatcan[0] != 0:
            ### co vat can
            vitri_vatcan = self.knn(self.left_lane, self.right_lane, np.array([thongtin_vatcan[3], thongtin_vatcan[4]]))
            if vitri_vatcan == -1:
                # vat can ben trai, re phai
                print('vat can ben trai')
                self.center_lane[1:, 0] = self.right_lane[1:, 0] - 15
            elif vitri_vatcan == 1:
                print('vat can ben phai')
                # vat can ben phai, re trai
                self.center_lane[1:, 0] = self.left_lane[1:, 0] + 15
        if debug:
            # point_image = cv.merge([birdview_image, birdview_image, birdview_image])
            point_image = cv.cvtColor(birdview_image,cv.COLOR_GRAY2RGB)
            center_lane = self.center_lane.astype(int)
            left_lane = self.left_lane.astype(int)
            right_lane = self.right_lane.astype(int)
            for i in range(len(center_lane)):
                if center_lane[i][0] < 0:
                    break
                cv.circle(point_image, tuple(center_lane[i]), 3, (0, 0, 255))
                cv.circle(point_image, tuple(left_lane[i]), 3, (255, 0, 0))
                cv.circle(point_image, tuple(right_lane[i]), 3, (0, 255, 0))
            # cv.circle(point_image, (thongtin_vatcan[3], thongtin_vatcan[4]), 3, (255, 255, 0))
            if self.obstacle.exist:
                cv.rectangle(point_image, (self.obstacle.xmin, self.obstacle.ymin),
                             (self.obstacle.xmax, self.obstacle.ymax), (0, 255, 127), 2)
            cv.imshow('p', point_image)
        return self.center_lane

    def timLanDuongTraiPhaiTai1Layer(self, layer, h, y, tam_duong_truoc):
        # todo : remove that find contor call
        _, contours, _ = cv.findContours(layer, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
        w = layer.shape[1]
        # layer_3kenh = np.zeros((layer.shape[0], w, 3), np.uint8)

        cnt_array = []
        ##### tim cac thong so cua cac contour de loc ######
        for i, cnt in enumerate(contours):
            cnt_area, t, b, l, r = self.timThongTinCuaMotContour(cnt)
            if cnt_area <= 0:
                continue
            cnt_array.append(
                [cnt_area, t[0], t[1] + y - self.layer_h, b[0], b[1] + y - self.layer_h, l[0], l[1] + y - self.layer_h,
                 r[0],
                 r[1] + y - self.layer_h])
        cnt_array = np.array(cnt_array)
        if len(cnt_array) == 0:
            ## layer trong
            x_l = tam_duong_truoc[0] - self.do_rong_duong
            left_point = np.array(
                [0, x_l, y - self.layer_h, x_l, y, x_l - 2, y - self.layer_h // 2, x_l + 2, y - self.layer_h // 2])
            x_r = tam_duong_truoc[0] + self.do_rong_duong
            right_point = np.array(
                [0, x_r, y - self.layer_h, x_r, y, x_r - 2, y - self.layer_h // 2, x_r + 2, y - self.layer_h // 2])
        elif y == h:
            ### truong hop layer dau tien
            ### tim cac contour nam ben trai
            left_cnts = cnt_array[cnt_array[:, 3] < w // 2 - 1]  # contour ben trai co diem cuc nam < w/2
            if len(left_cnts) > 0:
                left_point_index = left_cnts[:, 0].argmax()  # chon contour co dien tich lon nhat
                left_point = left_cnts[left_point_index].astype(np.int)
            else:
                # tu custom 1 diem nam giua ben trai
                x = w // 2 - 1 - self.do_rong_duong
                left_point = np.array(
                    [0, x, y - self.layer_h, x, y, x - 2, y - self.layer_h // 2, x + 2, y - self.layer_h // 2]).astype(
                    np.int)

            ### tim cac contour nam ben phai
            right_cnts = cnt_array[cnt_array[:, 3] > w // 2 - 1]
            if len(right_cnts) > 0:
                right_point_index = right_cnts[:, 0].argmax()
                right_point = right_cnts[right_point_index].astype(np.int)
            else:
                x = w // 2 - 1 + self.do_rong_duong
                right_point = np.array(
                    [0, x, y - self.layer_h, x, y, x - 2, y - self.layer_h // 2, x + 2, y - self.layer_h // 2]).astype(
                    np.int)
        else:
            ### tim contour nam ben trai co khoang cach den contour truoc do nho nhat
            if np.any(cnt_array[:, 3] < tam_duong_truoc[0]):
                left_cnts = cnt_array[cnt_array[:, 3] < tam_duong_truoc[0]]
                khoang_cach_den_cnt_truoc = (left_cnts[:, 3] - self.cnt_trai_layer_truoc[1]) ** 2 + (
                        left_cnts[:, 4] - self.cnt_phai_layer_truoc[2]) ** 2
                left_point_index = khoang_cach_den_cnt_truoc.argmin()
                left_point = left_cnts[left_point_index].astype(np.int)
            else:
                x = tam_duong_truoc[0] - self.do_rong_duong
                left_point = np.array(
                    [0, x, y - self.layer_h, x, y, x - 2, y - self.layer_h // 2, x + 2, y - self.layer_h // 2]).astype(
                    np.int)

            if np.any(cnt_array[:, 3] > tam_duong_truoc[0]):
                right_cnts = cnt_array[cnt_array[:, 3] > tam_duong_truoc[0]]
                khoang_cach_den_cnt_truoc = (right_cnts[:, 3] - self.cnt_phai_layer_truoc[1]) ** 2 + (
                        right_cnts[:, 4] - self.cnt_phai_layer_truoc[2]) ** 2
                right_point_index = khoang_cach_den_cnt_truoc.argmin()
                right_point = right_cnts[right_point_index].astype(np.int)
            else:
                x = tam_duong_truoc[0] + self.do_rong_duong
                right_point = np.array(
                    [0, x, y - self.layer_h, x, y, x - 2, y - self.layer_h // 2, x + 2, y - self.layer_h // 2]).astype(
                    np.int)
        self.center_point[:] = [(left_point[7] + right_point[5]) // 2, (left_point[8] + right_point[6]) // 2]
        self.cnt_trai_layer_truoc[:] = left_point
        self.cnt_phai_layer_truoc[:] = right_point

    def timThongTinCuaMotContour(self, cnt):
        cnt_area = cv.contourArea(cnt)
        xs, ys = np.rollaxis(cnt.reshape(-1, 2), 1)
        min_y_index = np.argmin(ys)
        max_y_index = np.argmax(ys)
        min_x_index = np.argmin(xs)
        max_x_index = np.argmax(xs)
        top_most = xs[min_y_index], ys[min_y_index]
        bot_most = xs[max_y_index], ys[max_y_index]
        left_most = xs[min_x_index], ys[min_x_index]
        right_most = xs[max_x_index], ys[max_x_index]

        return cnt_area, top_most, bot_most, left_most, right_most

    def calculateSteerAngle(self, centerPoints):

        negative_index = np.where(centerPoints == [-1, -1])[0]
        if len(negative_index) != 0:
            centerPoints = centerPoints[:np.where(centerPoints == [-1, -1])[0][0]]
        view = centerPoints[0, 0]  ## lay toa do x cua layer duoi cung lam view ? view gi ?
        discount = np.arange(len(centerPoints), 0, -1)

        angles = [self.calculateAngle(point, view) for point in centerPoints]
        angle = sum(discount * angles) / sum(discount)
        if abs(angle) < 1:
            angle *= 3
        else:
            angle *= 2
        return angle

    # tinh angle tren tung layer
    def calculateAngle(self, point, view):
        x = point[0]
        y = point[1]
        if -1 in [x, y]:
            print(x, y)
        if x == self.half_width - 1:
            return 0
        if y == self.h - 1:
            if x < self.half_width:
                return -90
            else:
                return 90
        dx = float(x - self.half_width)
        dy = self.h - y
        if dy == 0:
            if dx < 0:
                return -90
            elif dx > 0:
                return 90
            else:
                return 0
        if dx < 0:
            return -math.atan(-dx / dy) * 180 / math.pi
        return math.atan(dx / dy) * 180 / math.pi

    ### tinh toc do theo goc va toc do da publish truoc do
    def calculateSpeed(self, angle, old_speed):
        angle_process = abs(angle)
        # speed_out = 0
        if 0 <= angle_process < 1.5:
            max_angle = 1.5
            min_angle = 0
            delta_angle = max_angle - min_angle
            speed_out = (25.0 * (max_angle - angle_process) + 23.0 * (angle_process - min_angle)) / delta_angle

        elif 1.5 <= angle_process < 7.5:
            max_angle = 5.0
            min_angle = 1.5
            delta_angle = max_angle - min_angle
            speed_out = (23.0 * (max_angle - angle_process) + 21.0 * (angle_process - min_angle)) / delta_angle


        elif 7.5 <= angle_process < 15.0:
            max_angle = 15.0
            min_angle = 7.5
            delta_angle = max_angle - min_angle
            speed_out = (21.0 * (max_angle - angle_process) + 20.0 * (angle_process - min_angle)) / delta_angle
        elif 15.0 <= angle_process < 20.0:
            max_angle = 20.0
            min_angle = 15.0
            delta_angle = max_angle - min_angle
            speed_out = (20.0 * (max_angle - angle_process) + 19.0 * (angle_process - min_angle)) / delta_angle
        elif 20.0 <= angle_process < 25.0:
            max_angle = 25.0
            min_angle = 20.0
            delta_angle = max_angle - min_angle
            speed_out = (19.0 * (max_angle - angle_process) + 17.0 * (angle_process - min_angle)) / delta_angle
        elif 25.0 <= angle_process < 30.0:
            max_angle = 30.0
            min_angle = 25.0
            delta_angle = max_angle - min_angle
            speed_out = (17.0 * (max_angle - angle_process) + 16.0 * (angle_process - min_angle)) / delta_angle
        elif 30.0 <= angle_process < 35.0:
            max_angle = 35.0
            min_angle = 30.0
            delta_angle = max_angle - min_angle
            speed_out = (16.0 * (max_angle - angle_process) + 14.0 * (angle_process - min_angle)) / delta_angle
        elif 35.0 <= angle_process < 40.0:
            max_angle = 40.0
            min_angle = 35.0
            delta_angle = max_angle - min_angle
            speed_out = (14.0 * (max_angle - angle_process) + 12.0 * (angle_process - min_angle)) / delta_angle
        else:
            speed_out = 12.0
        # clip speed
        if speed_out - old_speed > 2:
            speed_out = old_speed + 2
        return speed_out


if __name__ == '__main__':
    cv2 = cv
    cap = cv2.VideoCapture(r"C:\Users\Binh Bum\Downloads\src\1554558905.98_rgb.avi")
    deph_cap = cv2.VideoCapture(r"C:\Users\Binh Bum\Downloads\src\1554558905.98_depth.avi")
    xuly = Xuly()
    index = 0
    while True:
        index += 1
        ret, mat = cap.read()
        _, deph = deph_cap.read()
        if not ret:
            cap.set(1, 0)
            deph_cap.set(1, 0)
            continue
        cv2.imshow('Orignal',mat)
        cv2.imshow('Orignal_depth',deph)
        deph = cv2.cvtColor(deph, cv2.COLOR_RGB2GRAY)
        angle, speed, chaucay = xuly.run(mat, deph, True, 0, 1)
        # angle, speed, chaucay = xuly.run(mat, deph, 0, 0, 1)

        k = cv2.waitKey(1)
        if k == 32:
            cv.waitKey(0)
