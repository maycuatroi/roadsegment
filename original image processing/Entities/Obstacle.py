import numpy as np


class Obstacle:
    def __init__(self):
        self.xmin = -1
        self.ymin = -1
        self.xmax = -1
        self.ymax = -1

        """
        bottom line = ymax
        """

        self.exist = False

    def distance2lane(self, points):
        xs, ys = np.rollaxis(points.reshape(-1, 2), 1)
        nearest_layer_index = np.argmin(np.abs(ys - self.ymax))
        nearest_x = xs[nearest_layer_index]

        return abs(nearest_x - self.xmin) + abs(nearest_x - self.xmax)

    def get_object_from_mat(self, mat):
        """
        Tim vat can va gan thong tin vat can
        :param mat: binary matrix or gray depth
        :return:
        """
        pass

    def vat_can_thuoc_lane_nao(self, leftlane, rightlane):
        left_distance = self.distance2lane(leftlane)
        right_distance = self.distance2lane(rightlane)
        if left_distance>right_distance:
            return 'right'
        else:
            return 'left'

