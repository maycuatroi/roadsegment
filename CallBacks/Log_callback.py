from keras.callbacks import Callback
import numpy as np
import cv2

class Log_Callback(Callback):
    def __init__(self, generator, log_path='../log', weights_path='../weights/weights.h5'):
        self.generator = generator
        self.log_patch = log_path
        self.weight_paths= weights_path

    def on_epoch_end(self, epoch, logs={}):
        bath = next(self.generator(1))
        image = bath[0][0]

        y = self.model.predict(np.array([image]))
        image *= 255.
        y *= 255.
        out = y[0]

        # segment[segment!=0] = 255.
        # print(image.shape,y[0][0].shape)
        # out = np.reshape(out,(100,100,3))
        image = cv2.resize(image,(300,300))
        # image = image.astype(np.uint8)
        # out = cv2.cvtColor(out,cv2.COLOR_GRAY2RGB)
        cv2.imwrite('{}/{}.jpg'.format(self.log_patch,epoch), np.concatenate((image,out),1))
        self.model.save_weights(self.weight_paths)
        return
